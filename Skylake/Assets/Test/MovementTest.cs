﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementTest : MonoBehaviour
{
    [Range(0f, 1f)]
    [SerializeField] float timeScale = 1f;
    [SerializeField] float start;
    [SerializeField] float speed;
    [SerializeField] Rigidbody2D rigid1;
    float rigidPosition;
    [SerializeField] Rigidbody2D rigid2;
    [SerializeField] Rigidbody2D rigid3;
    [SerializeField] Transform trans;
    float transPosition;

    private void Update()
    {
        Time.timeScale = timeScale;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            transPosition = rigidPosition = -start;
        }
        transPosition += speed * Time.deltaTime;
        
        trans.position = new Vector3(transPosition, trans.position.y, 0f);
    }

    private void FixedUpdate()
    {
        rigidPosition += speed * Time.fixedDeltaTime;
        rigid1.MovePosition(new Vector2(rigidPosition, rigid1.transform.position.y));
        rigid2.MovePosition(new Vector2(rigidPosition, rigid2.transform.position.y));
        rigid3.MovePosition(new Vector2(rigidPosition, rigid3.transform.position.y));
    }
}
