﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenPositionTest : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] RectTransform rectTransform;
    [SerializeField] public RectTransform CurrentCanvasRect;

    private void Update()
    {
        rectTransform.anchoredPosition = GetScreenPosition(target);
    }

    public Vector2 GetScreenPosition(Transform inputTransform)
    {
        Vector2 result = new Vector2();
        //result = Camera.main.WorldToScreenPoint(inputTransform.transform.position);
        result = CurrentCanvasRect.InverseTransformPoint(inputTransform.position) + new Vector3(CurrentCanvasRect.rect.width / 2f, CurrentCanvasRect.rect.height / 2f, 0f);
        return result;
    }
}
