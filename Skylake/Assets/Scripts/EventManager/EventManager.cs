﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public static event System.Action OnMenuSceneLoaded;
    public static event System.Action OnGameSceneLoaded;
    public static event System.Action OnHubSceneLoaded;
    public static event System.Action OnBattleStart;
    public static event System.Action OnBattleEnd;
    public static event System.Action<WindowOpenedEventData> OnWindowOpened;
    public static event System.Action<WindowClosedEventData> OnWindowClosed;
    public static event System.Action<SceneLoadedEventData> OnSceneLoaded;

    public static void Event(TypeOfEvent typeOfEvent)
    {
        switch (typeOfEvent)
        {
            case TypeOfEvent.MenuSceneLoaded:     OnMenuSceneLoaded?.Invoke();                   break;
            case TypeOfEvent.GameSceneLoaded:     OnGameSceneLoaded?.Invoke();                   break;
            case TypeOfEvent.HubSceneLoaded:      OnHubSceneLoaded?.Invoke();                    break;
            case TypeOfEvent.BattleStart:         OnBattleStart?.Invoke();                       break;
            case TypeOfEvent.BattleEnd:           OnBattleEnd?.Invoke();                         break;
            default: Debug.Log($"Event type {typeOfEvent} is not handled in event manager");     break;
        }
    }

    public static void Event<T>(T eventData) where T : EventDataBase
    {
        switch (eventData.typeOfEvent)
        {
            case TypeOfEvent.WindowOpened:    OnWindowOpened?.Invoke(eventData as WindowOpenedEventData);               break;
            case TypeOfEvent.WindowClosed:    OnWindowClosed?.Invoke(eventData as WindowClosedEventData);               break;
            case TypeOfEvent.SceneLoaded:     OnSceneLoaded?.Invoke(eventData as SceneLoadedEventData);                 break;
        }
    }
}

public class PlayerEventManager
{
    public static event System.Action OnBattleStart;
    public static event System.Action OnBattleEnd;
    public static event System.Action OnPlayerStartAim;
    public static event System.Action OnPlayerEndAim;
    public static event System.Action OnPlayerAimed;
    public static event System.Action OnPlayerNotAimed;
    public static event System.Action OnPlayerManualReload;
    public static event System.Action OnPlayerWeaponReloaded;
    public static event System.Action OnPlayerWeaponStartReload;
    public static event System.Action OnShowMagazine;
    public static event System.Action OnHideMagazine;
    public static event System.Action OnDropMagazine;
    public static event System.Action OnShootWeapon;
    public static event System.Action<WeaponEquippedEventData> OnWeaponEquipped;
    public static event System.Action<PlayerMovedEventData> OnPlayerMoved;

    public static void Event(TypeOfEvent typeOfEvent)
    {
        switch (typeOfEvent)
        {
            case TypeOfEvent.PlayerStartAim: OnPlayerStartAim?.Invoke();                        break;
            case TypeOfEvent.PlayerEndAim: OnPlayerEndAim?.Invoke();                            break;
            case TypeOfEvent.PlayerAimed: OnPlayerAimed?.Invoke();                              break;
            case TypeOfEvent.PlayerWeaponReloaded: OnPlayerWeaponReloaded?.Invoke();              break;
            case TypeOfEvent.PlayerNotAimed: OnPlayerNotAimed?.Invoke();                        break;
            case TypeOfEvent.ShowMagazine: OnShowMagazine?.Invoke();                            break;
            case TypeOfEvent.HideMagazine: OnHideMagazine?.Invoke();                            break;
            case TypeOfEvent.DropMagazine: OnDropMagazine?.Invoke();                            break;
            case TypeOfEvent.ShootWeapon: OnShootWeapon?.Invoke();                              break;
            case TypeOfEvent.PlayerWeaponStartReload: OnPlayerWeaponStartReload?.Invoke();      break;
            case TypeOfEvent.PlayerManualReload: OnPlayerManualReload?.Invoke();                break;
            default: Debug.Log($"Event type {typeOfEvent} is not handled in event manager");    break;
        }
    }

    public static void Event<T>(T eventData) where T : EventDataBase
    {
        switch (eventData.typeOfEvent)
        {
            case TypeOfEvent.WeaponEquipped: OnWeaponEquipped?.Invoke(eventData as WeaponEquippedEventData); break;
            case TypeOfEvent.PlayerMoved:    OnPlayerMoved?.Invoke(eventData as PlayerMovedEventData);       break;
        }
    }
}

public enum TypeOfEvent
{
    WindowOpened,
    WindowClosed,
    SceneLoaded,
    MenuSceneLoaded,
    HubSceneLoaded,
    GameSceneLoaded,
    PlayerStartAim,
    PlayerEndAim,
    PlayerAimed,
    PlayerNotAimed,
    BattleStart,
    BattleEnd,
    GamepadConnected,
    GamepadDisconnected,
    PlayerWeaponReloaded,
    PlayerManualReload,
    PlayerWeaponStartReload,
    WeaponEquipped,
    PlayerMoved,
    ShowMagazine,
    HideMagazine,
    DropMagazine,
    ShootWeapon,
}