﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowClosedEventData : EventDataBase
{
    public Window closedWindow;

    public WindowClosedEventData(Window closedWindow)
    {
        typeOfEvent = TypeOfEvent.WindowOpened;
        this.closedWindow = closedWindow;
    }
}