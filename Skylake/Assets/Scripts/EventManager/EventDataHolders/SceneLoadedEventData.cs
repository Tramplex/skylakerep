﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoadedEventData : EventDataBase
{
	public SceneType loadedScene;

	public SceneLoadedEventData(SceneType loadedScene)
	{
		typeOfEvent = TypeOfEvent.SceneLoaded;
		this.loadedScene = loadedScene;
	}
}
