﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponEquippedEventData : EventDataBase
{
    public Weapon equippedWeapon;

    public WeaponEquippedEventData(Weapon equippedWeapon)
    {
        typeOfEvent = TypeOfEvent.WeaponEquipped;
        this.equippedWeapon = equippedWeapon;
    }
}