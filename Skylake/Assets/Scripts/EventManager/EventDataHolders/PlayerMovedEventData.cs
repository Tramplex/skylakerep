﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovedEventData : EventDataBase
{
    public float move;

    public PlayerMovedEventData(float move)
    {
        typeOfEvent = TypeOfEvent.PlayerMoved;
        this.move = move;
    }
}
