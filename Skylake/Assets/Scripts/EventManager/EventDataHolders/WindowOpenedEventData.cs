﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowOpenedEventData : EventDataBase
{
    public Window openedWindow;

    public WindowOpenedEventData(Window openedWindow)
    {
        typeOfEvent = TypeOfEvent.WindowOpened;
        this.openedWindow = openedWindow;
    }
}