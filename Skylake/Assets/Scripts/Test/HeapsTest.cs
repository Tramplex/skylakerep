﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeapsTest : MonoBehaviour
{
    Heap<TestHeapItem> heap;
    List<TestHeapItem> items = new List<TestHeapItem>();

    private void Start()
    {
        heap = new Heap<TestHeapItem>(25000);
        for (int i = 0; i < 25000; i++)
        {
            items.Add(new TestHeapItem(UnityEngine.Random.Range(10f, 50f)));
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            heap.Clear();
            int usedItemsCount = Random.Range(0, 25000);
            for(int i = 0; i < usedItemsCount; i++)
            {
                int randomItemIndex = Random.Range(0, 25000);
                items[randomItemIndex].value = Random.Range(0f, 1000f);
                if (!heap.Contains(items[randomItemIndex]))
                {
                    heap.Add(items[randomItemIndex]);
                }
                else
                {
                    heap.UpdateItem(items[randomItemIndex]);
                }
            }
            Debug.Log($"Used {usedItemsCount} items");
        }
    }

    public static bool HeapIsCorrect<T>(Heap<T> heap) where T : IHeapItem<T>
    {
        bool result = true;
        for (int i = 0; i < heap.Count; i++)
        {
            int childIndexLeft = heap.items[i].HeapIndex * 2 + 1;
            int childIndexRight = heap.items[i].HeapIndex * 2 + 2;
            if (childIndexLeft < heap.Count && heap.items[childIndexLeft] != null
                && heap.items[childIndexLeft].CompareTo(heap.items[i]) > 0)
            {
                result = false;
                Debug.Log($"Fail on item index {childIndexLeft}");
                break;
            }
            if (childIndexRight < heap.Count && heap.items[childIndexRight] != null
                && heap.items[childIndexRight].CompareTo(heap.items[i]) > 0)
            {
                result = false;
                Debug.Log($"Fail on item index {childIndexRight}");
                break;
            }
            if(heap.items[i].HeapIndex != i)
            {
                Debug.LogError($"HeapIndex failed on index {i} is now {heap.items[i].HeapIndex}");
            }
        }
        return result;
    }
}

public class TestHeapItem : IHeapItem<TestHeapItem>
{
    public int HeapIndex { get; set; }
    public float value;

    public TestHeapItem(float value)
    {
        this.value = value;
    }

    public int CompareTo(TestHeapItem compareItem)
    {
        int compare = 0;
        if (value < compareItem.value) compare = 1;
        else if (value > compareItem.value) compare = -1;
        return compare;
    }
}