﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TestUpdates : MonoBehaviour
{
    public static TestUpdates Instance;
    Stopwatch watch;
    List<GameObject> testObjects = new List<GameObject>();
    [SerializeField] int objectsCount;
    List<IUpdatable> updateItems = new List<IUpdatable>();

    private void Awake()
    {
        Instance = this;
        watch = new Stopwatch();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            TestUsualUpdates();
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            TestCoroutineUpdates();
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            TestUpdateManager();
        }
        watch.Start();
        int updateItemsCount = updateItems.Count;
        for(int i = 0; i < updateItemsCount; i++)
        {
            updateItems[i].OnUpdate();
        }
    }

    private void LateUpdate()
    {
        watch.Stop();
        UnityEngine.Debug.Log($"Frame time {watch.ElapsedMilliseconds}");
        watch.Reset();
    }

    public void AddUpdateItem(IUpdatable updateItem)
    {
        updateItems.Add(updateItem);
    }

    void TestUsualUpdates()
    {
        DestroyTestObjects();
        for (int i = 0; i < objectsCount; i++)
        {
            var testObject = new GameObject("UsualUpdateTest");
            testObject.transform.SetParent(transform);
            testObject.AddComponent<UsualUpdateTestItem>();
            testObjects.Add(testObject);
        }
    }

    void TestCoroutineUpdates()
    {
        DestroyTestObjects();
        for (int i = 0; i < objectsCount; i++)
        {
            var testObject = new GameObject("CoroutineTest");
            testObject.transform.SetParent(transform);
            testObject.AddComponent<CoroutineUpdateTestItem>();
            testObjects.Add(testObject);
        }
    }

    void TestUpdateManager()
    {
        DestroyTestObjects();
        for (int i = 0; i < objectsCount; i++)
        {
            var testObject = new GameObject("UpdateManagerTest");
            testObject.transform.SetParent(transform);
            testObject.AddComponent<UpdateTestItem>();
            testObjects.Add(testObject);
        }
    }

    void DestroyTestObjects()
    {
        updateItems.Clear();
        for (int i = 0; i < testObjects.Count; i++)
        {
            Destroy(testObjects[i]);
        }
        testObjects.Clear();
    }
}

public class UsualUpdateTestItem : MonoBehaviour
{
    private void Update()
    {
        
    }
}

public class UpdateTestItem : MonoBehaviour, IUpdatable
{
    public bool SubscribedForUpdates { get; set; }

    private void Start()
    {
        TestUpdates.Instance.AddUpdateItem(this);
    }

    public void OnUpdate()
    {

    }
}

public class CoroutineUpdateTestItem : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(UpdateCoroutine());
    }

    IEnumerator UpdateCoroutine()
    {
        while (true) 
        {
            yield return null;
        }
    }
}