﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TestLocalVars : MonoBehaviour
{
    Stopwatch watch = new Stopwatch();
    public int testsCount;

    double testFloat;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            watch.Restart();
            for(int i = 0; i < testsCount; i++)
            {
                double testFloatBad = 0.15;
            }
            watch.Stop();
            UnityEngine.Debug.Log($"Local declaration took {watch.ElapsedMilliseconds} ms");
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            watch.Restart();
            for (int i = 0; i < testsCount; i++)
            {
                testFloat = 0.15;
            }
            watch.Stop();
            UnityEngine.Debug.Log($"Global declaration took {watch.ElapsedMilliseconds} ms");
        }
    }
}
