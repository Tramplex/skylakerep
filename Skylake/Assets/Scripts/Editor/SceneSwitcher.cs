﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class SceneSwitcher : MonoBehaviour
{
	[MenuItem("MyMenu/Load pre game &1")]
	static void LoadPreGameScene()
	{
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.SaveOpenScenes();
        }
        EditorSceneManager.OpenScene("Assets/Scenes/PreGameScene.unity");
	}

	[MenuItem("MyMenu/Load menu &2")]
	static void LoadMenuScene()
    {
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.SaveOpenScenes();
        }
        EditorSceneManager.OpenScene("Assets/Scenes/MenuScene.unity");
	}

	[MenuItem("MyMenu/Load game &3")]
	static void LoadGameScene()
    {
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.SaveOpenScenes();
        }
        EditorSceneManager.OpenScene("Assets/Scenes/GameScene.unity");
	}

	[MenuItem("MyMenu/Load hub &4")]
	static void LoadHubScene()
    {
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.SaveOpenScenes();
        }
        EditorSceneManager.OpenScene("Assets/Scenes/HubScene.unity");
	}

	[MenuItem("MyMenu/Load ui constroction &5")]
	static void LoadUIConstructionScene()
    {
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.SaveOpenScenes();
        }
        EditorSceneManager.OpenScene("Assets/Scenes/UIConstructionScene.unity");
	}
}
