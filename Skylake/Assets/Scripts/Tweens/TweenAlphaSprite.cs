﻿//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2016 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;
using TMPro;
using UnityEngine.UI;
/// <summary>
/// Tween the object's alpha. Works with both UI widgets as well as renderers.
/// </summary>

[AddComponentMenu("NGUI/Tween/Tween Alpha Sprite")]
public class TweenAlphaSprite : UITweener
{
    [Range(0f, 1f)] public float from = 1f;
    [Range(0f, 1f)] public float to = 1f;
    public bool _childInclude;

    bool mCached = false;
    //	UIRect mRect;
    //	Material mMat;
    //	SpriteRenderer mSr;
    [SerializeField] float _currentAlpha = 0;
    [SerializeField] private SpriteRenderer _thisSprite;
    [SerializeField] private Color _thisColor;
    [SerializeField] private SpriteRenderer[] _spriteList;
    [SerializeField] private Color[] _spriteColor;

    [System.Obsolete("Use 'value' instead")]
    public float alpha { get { return this.value; } set { this.value = value; } }

    public override void PlayForward()
    {
        //ResetCache();
        base.PlayForward();
    }

    public override void PlayReverse()
    {
        //ResetCache();
        base.PlayReverse();
    }

    void Cache()
    {
        _thisSprite = GetComponent<SpriteRenderer>();

        if (_childInclude)
        {
            _spriteList = GetComponentsInChildren<SpriteRenderer>(true);
            _spriteColor = new Color[_spriteList.Length];
        }

        if (_childInclude)
        {
            for (int i = 0; i < _spriteList.Length; i++)
            {
                _spriteColor[i] = _spriteList[i].color;
            }
        }
        mCached = true;
        //	mRect = GetComponent<UIRect>();
        //	mSr = GetComponent<SpriteRenderer>();

        //	if (mRect == null && mSr == null)
        //	{
        //		Renderer ren = GetComponent<Renderer>();
        //		if (ren != null) mMat = ren.material;
        //		if (mMat == null) mRect = GetComponentInChildren<UIRect>();
        //	}
    }

    public void ResetCache()
    {
        mCached = false;
        Cache();
    }

    /// <summary>
    /// Tween's current value.
    /// </summary>
    Color tempColor;
    private void SetAlpha(float setAlpha)
    {
        if (_thisSprite != null)
        {
            _thisColor = _thisSprite.color;
            _thisColor.a = setAlpha;
            _thisSprite.color = _thisColor;
        }

        if (_childInclude)
        {
            for (int i = 0; i < _spriteList.Length; i++)
            {
                tempColor = _spriteList[i].color;
                tempColor.a = setAlpha;
                _spriteList[i].color = tempColor;
            }
        }

        _currentAlpha = setAlpha;
    }


    public float value
    {
        get
        {

            return _currentAlpha;
        }
        set
        {
            _currentAlpha = value;

            if (!mCached) Cache();

            SetAlpha(value);
        }
    }

    /// <summary>
    /// Tween the value.
    /// </summary>

    protected override void OnUpdate(float factor, bool isFinished)
    {
        value = Mathf.Lerp(from, to, factor);
    }

    /// <summary>
    /// Start the tweening operation.
    /// </summary>



    static public TweenAlpha Begin(GameObject go, float duration, float alpha)
    {
        TweenAlpha comp = UITweener.Begin<TweenAlpha>(go, duration);
        comp.from = comp.value;
        comp.to = alpha;
        if (duration <= 0f)
        {
            comp.Sample(1f, true);
            comp.enabled = false;
        }
        return comp;
    }
    [ContextMenu("Cache")]
    void CacheSet()
    {
        mCached = false;
        Cache();
    }
    public override void SetStartToCurrentValue() { from = value; }
    public override void SetEndToCurrentValue() { to = value; }
}
