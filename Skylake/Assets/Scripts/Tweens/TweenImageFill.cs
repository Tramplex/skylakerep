﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (Image))]
public class TweenImageFill : MonoBehaviour
{
    [SerializeField] bool unscaledTime = false;
    [SerializeField] bool playOnAwake = false;
    [SerializeField] float startDelay = 0f;
    [SerializeField] float from = 0f;
    [SerializeField] float to = 1f;
    [SerializeField] Image targetImage;
    [SerializeField] public float animationTime = 1f;
    Coroutine animationCoroutine;
    public bool isPlaying;
    float duration;

    private void OnEnable()
    {
        if (playOnAwake)
        {
            PlayAnimation();
        }
        else
        {
            targetImage.fillAmount = from;
        }
    }

    private void OnDisable()
    {
        ResetToBeggining();
    }

    public void PlayAnimation()
    {
        targetImage.fillAmount = from;
        isPlaying = true;
        time = 0f;
    }

    public void ResetToEnding()
    {
        targetImage.fillAmount = to;
        isPlaying = false;
    }

    public void ResetToBeggining()
    {
        targetImage.fillAmount = from;
        isPlaying = false;
    }

    public void PlayInstantly()
    {
        targetImage.fillAmount = to;
        isPlaying = false;
    }

    float time = 0f;
    private void Update()
    {
        if (!isPlaying) return;
        if(time > startDelay)
        {
            if(time < startDelay + animationTime)
            {
                targetImage.fillAmount = Mathf.Lerp(from, to, (time - startDelay) / animationTime);
            }
            else
            {
                isPlaying = false;
                targetImage.fillAmount = to;
            }
        }
        time += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
    }
}
