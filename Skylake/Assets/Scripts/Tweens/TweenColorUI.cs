﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TweenColorUI : UITweener
{
    [SerializeField] Color from;
    [SerializeField] Color to;
	public bool childInclude;
	[SerializeField] private Image _thisImage;
	[SerializeField] private Image[] imageList;
	[SerializeField] private TextMeshProUGUI _thisText;
	[SerializeField] private TextMeshProUGUI[] textList;
	bool cached = false;

	public void Cache() 
    {
		_thisImage = GetComponent<Image>();
		_thisText = GetComponent<TextMeshProUGUI>();

		if (childInclude)
		{
			imageList = GetComponentsInChildren<Image>(true);
			textList = GetComponentsInChildren<TextMeshProUGUI>(true);
		}
		cached = true;
	}

    protected override void OnUpdate(float factor, bool isFinished)
    {
		if (!cached) Cache();
		Color currentColor = Color.Lerp(from, to, factor);
		SetColor(currentColor);
    }

	void SetColor(Color color)
	{
		if(_thisImage != null)
		{
			color.a = _thisImage.color.a;
			_thisImage.color = color;
		}
		if(_thisText != null)
		{
			color.a = _thisText.color.a;
			_thisText.color = color;
		}
		if (childInclude)
		{
			for (int i = 0; i < imageList.Length; i++)
			{
				color.a = imageList[i].color.a;
				imageList[i].color = color;
			}

			for (int i = 0; i < textList.Length; i++)
			{
				color.a = textList[i].color.a;
				textList[i].color = color;
			}
		}
	}
}
