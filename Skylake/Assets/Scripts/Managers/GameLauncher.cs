﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLauncher : MonoBehaviour
{
    [SerializeField] ProgressBar menuLoadingProgress;
    [SerializeField] GameManager gameManager;
    AsyncOperation menuLoadOperation;

    private void Start()
    {
        gameManager.Initialize();
        menuLoadOperation = SceneManager.LoadSceneAsync(Constants.MENU_SCENE_NAME);
    }

    private void Update()
    {
        menuLoadingProgress.SetValue(menuLoadOperation.progress);
    }
}
