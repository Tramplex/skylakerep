﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TheSurface/New SettingsData")]
public class SettingsData : ScriptableObject
{
    public bool AutoReloadInBattle;

    public void Init()
    {
        int firstLaunch = PlayerPrefs.GetInt(Constants.FIRST_LAUNCH_STRING);
        if(firstLaunch == 0)
        {
            PlayerPrefs.SetInt(Constants.AUTO_RELOAD, 1);
            PlayerPrefs.SetInt(Constants.FIRST_LAUNCH_STRING, 1);
        }
    }
}
