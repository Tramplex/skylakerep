﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowManager : MonoBehaviour
{
    [SerializeField] List<GameObject> windowsPrefabs = new List<GameObject>();
    public Canvas CurrentCanvas { get; private set; }
    public RectTransform CurrentCanvasRect { get; private set; }
    [SerializeField] GameObject loadingWindowPrefab;
    public LoadingWindow loadingWindow { get; private set; }
    GraphicRaycaster currentCanvasRaycaster;
    WindowsHolder windowsHolder;
    Dictionary<int, Window> instantiatedWindows = new Dictionary<int, Window>();
    RectTransform baseWindowHolder;
    public float CanvasScaleFactor { get; private set; }

    public void Initialize()
    {
        SceneLoaded(new SceneLoadedEventData(SceneType.PreGame));
        EventManager.OnSceneLoaded += SceneLoaded;
    }

    public void ToggleInteractionBlock(bool blockActive)
    {
        currentCanvasRaycaster.enabled = !blockActive;
    }

    public Window OpenWindow(WindowType windowToOpen)
    {
        int windowIndex = (int)windowToOpen;
        if (windowsPrefabs.Count <= windowIndex)
        {
            Debug.Log($"No window prefab {windowToOpen} in prefabsList");
            return null;
        }
        if (!instantiatedWindows.ContainsKey(windowIndex))
        {
            var windowObject = Instantiate(windowsPrefabs[windowIndex], baseWindowHolder);
            Window openedWindow = windowObject.GetComponent<Window>();
            windowObject.transform.SetParent(windowsHolder.GetWindowHolder(openedWindow.layer));
            openedWindow.Initialize();
            instantiatedWindows.Add(windowIndex, openedWindow);
        }
        var window = instantiatedWindows[windowIndex];
        window.OnOpen();
        EventManager.Event(new WindowClosedEventData(window));
        return window;
    }

    public void CloseWindow(WindowType windowToClose)
    {
        int windowIndex = (int)windowToClose;
        if (instantiatedWindows.ContainsKey(windowIndex))
        {
            var window = instantiatedWindows[windowIndex];
            window.OnClose();
            EventManager.Event(new WindowClosedEventData(window));
        }
        else
        {
            Debug.Log($"There is no {windowToClose} instantiated");
        }
    }

    public Vector2 GetCanvasRelatedPosition(Transform inputTransform)
    {
        Vector2 result = new Vector2();
        result = CurrentCanvasRect.InverseTransformPoint(inputTransform.position);
        return result;
    }

    public Vector2 GetScreenPosition(Transform inputTransform)
    {
        Vector2 result = new Vector2();
        result = CurrentCanvasRect.InverseTransformPoint(inputTransform.position) + new Vector3(CurrentCanvasRect.rect.width / 2f, CurrentCanvasRect.rect.height / 2f, 0f);
        return result;
    }

    void SceneLoaded(SceneLoadedEventData eventData)
    {
        CurrentCanvas = GameObject.FindGameObjectWithTag(Constants.MAIN_CANVAS_TAG).GetComponent<Canvas>();
        CurrentCanvasRect = CurrentCanvas.GetComponent<RectTransform>();
        currentCanvasRaycaster = CurrentCanvas.GetComponent<GraphicRaycaster>();
        loadingWindow = Instantiate(loadingWindowPrefab, CurrentCanvasRect).GetComponent<LoadingWindow>();
        loadingWindow.FinishedLoading();
        windowsHolder = FindObjectOfType<WindowsHolder>();
        instantiatedWindows.Clear();
        if (windowsHolder == null) Debug.Log("Didnt find WindowsHolder");
        else baseWindowHolder = windowsHolder.GetWindowHolder(0);
        StartCoroutine(WaitCanvasInit(eventData.loadedScene));
    }

    IEnumerator WaitCanvasInit(SceneType loadedScene)
    {
        yield return null;
        CanvasScaleFactor = CurrentCanvasRect.rect.height / Screen.height;
        switch (loadedScene)
        {
            case SceneType.Menu:
                OpenWindow(WindowType.MainMenu);
                break;
        }
    }
}

public enum WindowType
{
    MainMenu,
    InGame,
}
