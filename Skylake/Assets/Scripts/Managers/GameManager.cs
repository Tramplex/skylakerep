﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    [SerializeField] SettingsData settingsData;
    public static SettingsData SettingsData { get; private set; }
    [SerializeField] BattleManager battleManager;
    public static BattleManager BattleManager { get; private set; }
    [SerializeField] InputManager inputManager;
    public static InputManager InputManager { get; private set; }
    [SerializeField] UpdateManager updateManager;
    public static UpdateManager UpdateManager { get; private set; }
    [SerializeField] WindowManager windowManager;
    public static WindowManager WindowManager { get; private set; }
    public string CurrentLoadedSceneName { get; private set; }
    public SceneType CurrentLoadedScene { get; private set; }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    public void Initialize()
    {
        SceneManager.sceneLoaded += SceneLoaded;
        BattleManager = battleManager;
        InputManager = inputManager;
        WindowManager = windowManager;
        SettingsData = settingsData;
        WindowManager.Initialize();
        UpdateManager = updateManager;
    }

    void SceneLoaded(Scene scene, LoadSceneMode mode)
    {
        switch (scene.name)
        {
            case Constants.MENU_SCENE_NAME:
                EventManager.Event(TypeOfEvent.MenuSceneLoaded);
                CurrentLoadedScene = SceneType.Menu;
                break;
            case Constants.HUB_SCENE_NAME: 
                EventManager.Event(TypeOfEvent.HubSceneLoaded);
                CurrentLoadedScene = SceneType.Hub;
                break;
            case Constants.GAME_SCENE_NAME:
                EventManager.Event(TypeOfEvent.GameSceneLoaded);
                CurrentLoadedScene = SceneType.Game;
                break;
        }
        EventManager.Event(new SceneLoadedEventData(CurrentLoadedScene));
        CurrentLoadedSceneName = scene.name;
    }

    public void LoadScene(string sceneName)
    {
        WindowManager.ToggleInteractionBlock(true);
        WindowManager.loadingWindow.StartLoading();
        switch (CurrentLoadedSceneName)
        {
            case Constants.HUB_SCENE_NAME:
            case Constants.GAME_SCENE_NAME:
                BattleManager.BattleFinished();
                break;
        }
        StartCoroutine(WaitStartLoadingScene(sceneName));
    }

    IEnumerator WaitStartLoadingScene(string sceneName)
    {
        yield return CoroutineTools.GetYield(1.5f);
        SceneManager.LoadSceneAsync(sceneName);
    }
}

public enum SceneType
{
    NotDefined,
    PreGame,
    Menu,
    Hub,
    Game
}
