﻿using System.Collections;
using System.Collections.Generic;
using TS.UI;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
	GameJoystick moveJoystick;
	GameJoystick aimJoystick;

    public static bool GamepadConnected { get; private set; }

	public Vector2 moveInput { get; private set; }
	public Vector2 aimInput { get; private set; }

	public event System.Action OnPlayerMoveStart;
	public event System.Action OnPlayerMoveEnd;
	public event System.Action OnPlayerRunStart;
	public event System.Action OnPlayerRunEnd;
	public event System.Action OnPlayerAimStart;
	public event System.Action OnPlayerAimEnd;

    Coroutine checkGamepadCoroutine;

    private void Awake()
    {
        GamepadConnected = false;
        checkGamepadCoroutine = StartCoroutine(CheckGamepadConnection());
        EventManager.OnBattleEnd += BattleEnd;
    }

    public void SetPlayerInput(GameJoystick moveJoystick, GameJoystick aimJoystick, Button reloadButton, GameButton runButton)
	{
        this.moveJoystick = moveJoystick;
        moveJoystick.OnOutputStart += PlayerMoveStart;
		moveJoystick.OnOutputEnd += PlayerMoveEnd;
		moveJoystick.Enable();
        this.aimJoystick = aimJoystick;
		aimJoystick.OnTouchDown += runButton.Disable;
		aimJoystick.OnOutputStart += PlayerAimStart;
		aimJoystick.OnOutputEnd += PlayerAimEnd;
		aimJoystick.OnOutputEnd += runButton.Enable;
        aimJoystick.Enable();
        OnPlayerRunStart += aimJoystick.Disable;
        OnPlayerRunEnd += aimJoystick.Enable;
        reloadButton.onClick.RemoveAllListeners();
        reloadButton.onClick.AddListener(PlayerReloadWeapon);
        runButton.ClearEvents();
        runButton.OnDown += PlayerStartRunning;
        runButton.OnUp += PlayerStopRunning;
        runButton.Enable();
    }

    void PlayerStartRunning()
    {
        OnPlayerRunStart?.Invoke();
    }

    void PlayerStopRunning()
    {
        OnPlayerRunEnd?.Invoke();
    }

    void PlayerReloadWeapon()
    {
        PlayerEventManager.Event(TypeOfEvent.PlayerManualReload);
    }

	void PlayerMoveStart()
	{
        if (!GamepadConnected)
        {
            moveInput = moveJoystick.output;
        }
        OnPlayerMoveStart?.Invoke();
	}

	void PlayerMoveEnd()
	{
		OnPlayerMoveEnd?.Invoke();
	}

	void PlayerAimStart()
	{
        if (!GamepadConnected)
        {
            aimInput = aimJoystick.output;
        }
        PlayerEventManager.Event(TypeOfEvent.PlayerStartAim);
		OnPlayerAimStart?.Invoke();
	}

	void PlayerAimEnd()
	{
        PlayerEventManager.Event(TypeOfEvent.PlayerEndAim);
		OnPlayerAimEnd?.Invoke();
	}

    void BattleEnd()
    {
        if(moveJoystick != null)
        {
            moveJoystick.OnOutputStart -= PlayerMoveStart;
            moveJoystick.OnOutputEnd -= PlayerMoveEnd;
        }
        if(aimJoystick != null)
        {
            aimJoystick.OnOutputStart -= PlayerAimStart;
            aimJoystick.OnOutputEnd -= PlayerAimEnd;
        }
        OnPlayerMoveStart = null;
	    OnPlayerMoveEnd = null;
	    OnPlayerRunStart = null;
	    OnPlayerRunEnd = null;
	    OnPlayerAimStart = null;
	    OnPlayerAimEnd = null;
    }

    private void Update()
    {
        #region Input control
#if UNITY_EDITOR
        if (moveJoystick != null)
        {
            Vector2 newInput = Vector2.zero;
            if (Input.GetKey(KeyCode.W))
            {
                newInput.y += 1;
            }
            if (Input.GetKey(KeyCode.S))
            {
                newInput.y -= 1;
            }
            if (Input.GetKey(KeyCode.A))
            {
                newInput.x -= 1;
            }
            if (Input.GetKey(KeyCode.D))
            {
                newInput.x += 1;
            }
            if (moveJoystick.output.x == 0 && moveJoystick.output.y == 0 && moveInput.x == 0 && moveInput.y == 0 && (newInput.x != 0 || newInput.y != 0))
            {
                PlayerMoveStart();
            }
            else if (moveJoystick.output.x == 0 && moveJoystick.output.y == 0 && (moveInput.x != 0 || moveInput.y != 0) && newInput.x == 0 && newInput.y == 0)
            {
                PlayerMoveEnd();
            }
            if (newInput.x == 0 && newInput.y == 0)
            {
                moveInput = moveJoystick.output;
            }
            else
            {
                moveInput = newInput.normalized;
            }
        }

        if (aimJoystick != null)
        {
            aimInput = aimJoystick.output;
        }
        else
        {
            aimInput = Vector2.zero;
        }
        #elif UNITY_ANDROID
        if (GamepadConnected)
        {
            GamePadInput();
        }
        else
        {
            InGameInput();
        }
#endif
        #endregion
    }

    void InGameInput()
    {
        if (moveJoystick != null)
        {
            moveInput = moveJoystick.output;
        }
        else
        {
            moveInput = Vector2.zero;
        }

        if (aimJoystick != null)
        {
            aimInput = aimJoystick.output;
        }
        else
        {
            aimInput = Vector2.zero;
        }
    }

    bool gamepadMoveStarted = false;
    bool gamepadAimStarted = false;
    void GamePadInput()
    {
        moveInput = new Vector2(Input.GetAxis(Constants.leftHorizontalAxis), Input.GetAxis(Constants.leftVerticalAxis));
        if(moveInput.x > 0.001f || moveInput.x < -0.001f || moveInput.y > 0.001f || moveInput.y < -0.001f)
        {
            if (!gamepadMoveStarted)
            {
                gamepadMoveStarted = true;
                PlayerMoveStart();
            }
        }
        else
        {
            if (gamepadMoveStarted)
            {
                gamepadMoveStarted = false;
                PlayerMoveEnd();
            }
        }
        aimInput = new Vector2(Input.GetAxis(Constants.rightHorizontalAxis), Input.GetAxis(Constants.rightVerticalAxis));
        if (aimInput.x > 0.001f || aimInput.x < -0.001f || aimInput.y > 0.001f || aimInput.y < -0.001f)
        {
            if (!gamepadAimStarted)
            {
                gamepadAimStarted = true;
                PlayerAimStart();
            }
        }
        else
        {
            if (gamepadAimStarted)
            {
                gamepadAimStarted = false;
                PlayerAimEnd();
            }
        }
    }

    IEnumerator CheckGamepadConnection()
    {
        WaitForSecondsRealtime waitCheck = new WaitForSecondsRealtime(2f);
        string[] joystickNames;
        int joystickNamesLength;
        bool joystickConnected;
        while (true)
        {
            yield return waitCheck;
            joystickConnected = false;
            joystickNames = Input.GetJoystickNames();
            joystickNamesLength = joystickNames.Length;
            if (joystickNamesLength > 0)
            {
                for (int i = 0; i < joystickNamesLength; i++)
                {
                    if (!string.IsNullOrEmpty(joystickNames[i]))
                    {
                        joystickConnected = true;
                        break;
                    }
                }
            }
            if(joystickConnected && !GamepadConnected)
            {
                GamepadConnected = true;
                Debug.Log("Gamepad connected!");
                EventManager.Event(TypeOfEvent.GamepadConnected);
            }
            else if (!joystickConnected && GamepadConnected)
            {
                GamepadConnected = false;
                EventManager.Event(TypeOfEvent.GamepadDisconnected);
            }
        }
    }
}
