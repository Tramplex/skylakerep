﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScalerFitter : MonoBehaviour
{
    float referenceScreenWidth { get { return canvasScaler.referenceResolution.x; } }
    float referenceScreenHeight { get { return canvasScaler.referenceResolution.y; } }
    CanvasScaler _canvasScaler;
    CanvasScaler canvasScaler
    {
        get
        {
            return _canvasScaler != null ? _canvasScaler : _canvasScaler = gameObject.GetComponent<CanvasScaler>();
        }
    }
    bool isLandscape { get { return referenceScreenWidth > referenceScreenHeight; } }
    float referenceAspectRatio
    {
        get
        {
            return isLandscape ? (float)referenceScreenWidth / referenceScreenHeight : (float)referenceScreenHeight / referenceScreenWidth;
        }
    }
    float currentAspectRatio { get { return isLandscape ? (float)Screen.width / Screen.height : (float)Screen.height / Screen.width; } }

    private void Awake()
    {
        CanvasScalerFitting();
    }

    private void CanvasScalerFitting()
    {
        CanvasScaler cScaler = gameObject.GetComponent<CanvasScaler>();
        if (cScaler)
        {
            if (referenceAspectRatio < currentAspectRatio)
            {
                cScaler.matchWidthOrHeight = 1;
            }
            else
            {
                cScaler.matchWidthOrHeight = 0;
            }
        }
    }
}
