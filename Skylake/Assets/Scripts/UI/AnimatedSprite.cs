﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedSprite : MonoBehaviour, IUpdatable
{
    public bool SubscribedForUpdates { get; set; }
    [SerializeField] List<Sprite> sprites = new List<Sprite>();
    [SerializeField] SpriteRenderer myRenderer;
    /// <summary>
    /// Works for loop mode.
    /// </summary>
    [SerializeField] float lifeTime;
    [SerializeField] bool loop;
    [SerializeField] float fadeTime;
    [SerializeField] int targetFPS;
    float timeBetweenFrames;
    int framesCount;
    int currentFrame;
    Color spriteColor;

    private void Awake()
    {
        timeBetweenFrames = 1f / targetFPS;
        framesCount = sprites.Count;
        spriteColor = myRenderer.color;
    }

    void OnEnable()
    {
        PlayAnimation();
    }

    void OnDisable()
    {
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
    }

    public void PlayAnimation()
    {
        currentFrame = 0;
        spriteColor.a = 1f;
        myRenderer.color = spriteColor;
        myRenderer.sprite = null;
        removeDelayTimer = 0;
        betweenFrameTimer = 0;
        myRenderer.sprite = sprites[0];
        GameManager.UpdateManager.SubscribeForUpdate(this);
    }

    float removeDelayTimer;
    float betweenFrameTimer;
    public void OnUpdate()
    {
        if (currentFrame < framesCount - 1)
        {
            betweenFrameTimer += Time.deltaTime;
            if (betweenFrameTimer >= timeBetweenFrames)
            {
                betweenFrameTimer -= timeBetweenFrames;
                currentFrame++;
                myRenderer.sprite = sprites[currentFrame];
            }
        }
        else
        {
            removeDelayTimer += Time.deltaTime;
            if (loop && removeDelayTimer >= lifeTime || !loop)
            {
                float spriteAlpha = fadeTime - (removeDelayTimer - lifeTime);
                spriteColor.a = spriteAlpha;
                myRenderer.color = spriteColor;
            }
        }
    }
}
