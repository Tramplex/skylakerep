﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] Image progressBarImage;

    private void Awake()
    {
        SetValue(0f);
    }

    public void SetValue(float value)
    {
        progressBarImage.fillAmount = value;
    }
}
