﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuWindow : Window
{
    public override void OnOpen()
    {
        base.OnOpen();
    }

    public override void OnClose()
    {
        base.OnClose();
    }

    public void StartGame()
    {
        GameManager.Instance.LoadScene(Constants.HUB_SCENE_NAME);
    }
}
