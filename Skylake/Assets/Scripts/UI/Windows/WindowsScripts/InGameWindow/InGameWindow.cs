﻿using System.Collections;
using System.Collections.Generic;
using TS.UI;
using UnityEngine;
using UnityEngine.UI;

public class InGameWindow : Window
{
    [SerializeField] PlayerHealthbar playerHealthbar;
    [SerializeField] GameJoystick leftJoystick;
    [SerializeField] GameJoystick rightJoystick;
    [SerializeField] Button reloadButton;
    [SerializeField] GameButton runButton;

    public void Init()
    {
        leftJoystick.Init();
        rightJoystick.Init();
        GameManager.InputManager.SetPlayerInput(leftJoystick, rightJoystick, reloadButton, runButton);
        playerHealthbar.Init(BattleManager.Player.PlayerHealth);
    }
}
