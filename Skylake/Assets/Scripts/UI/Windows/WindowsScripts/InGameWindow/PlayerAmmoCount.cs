﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerAmmoCount : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI ammoCountText;
    [SerializeField] TweenImageFill reloadProgressImageTween;
    [SerializeField] TweenColorUI ammoCountLowTween;
    Weapon currentWeapon;

    private void Awake()
    {
        ammoCountText.text = string.Empty;
        reloadProgressImageTween.ResetToBeggining();
        PlayerEventManager.OnWeaponEquipped += PlayerEquippedWeapon;
    }

    private void OnDestroy()
    {
        PlayerEventManager.OnWeaponEquipped -= PlayerEquippedWeapon;
    }

    void PlayerEquippedWeapon(WeaponEquippedEventData eventData)
    {
        if(currentWeapon != null)
        {
            currentWeapon.OnShotFired -= UpdateAmmoCount;
            currentWeapon.OnWeaponStartReload -= WeaponStartReload;
            currentWeapon.OnWeaponReloaded -= WeaponReloaded;
        }
        currentWeapon = eventData.equippedWeapon;
        currentWeapon.OnShotFired += UpdateAmmoCount;
        currentWeapon.OnWeaponStartReload += WeaponStartReload;
        currentWeapon.OnWeaponReloaded += WeaponReloaded;
        reloadProgressImageTween.animationTime = currentWeapon.ReloadTime;
        reloadProgressImageTween.ResetToEnding();
        UpdateAmmoCount();
    }

    void WeaponStartReload()
    {
        reloadProgressImageTween.PlayAnimation();
        UpdateAmmoCount();
    }

    void WeaponReloaded()
    {
        //reloadProgressImageTween.ResetToBeggining();
        UpdateAmmoCount();
    }

    void UpdateAmmoCount()
    {
        ammoCountText.text = currentWeapon.CurrentAmmo.ToString();
        if ((float)currentWeapon.CurrentAmmo / currentWeapon.MaxAmmo < Constants.LOW_AMMO_THRESHOLD)
        {
            if (!ammoCountLowTween.enabled) ammoCountLowTween.PlayForward();
        }
        else if (ammoCountLowTween.enabled)
        {
            ammoCountLowTween.ResetToBeginning();
            ammoCountLowTween.enabled = false;
        }
    }
}
