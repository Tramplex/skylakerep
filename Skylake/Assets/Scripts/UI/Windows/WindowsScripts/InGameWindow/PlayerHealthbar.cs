﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerHealthbar : MonoBehaviour
{
    [SerializeField] ProgressBar progressBar;
    [SerializeField] TextMeshProUGUI healthText;
    HealthController healthController;

    public void Init(HealthController healthController)
    {
        this.healthController = healthController;
        healthController.OnHealthChange += HealthChange;
        HealthChange(healthController.CurrentHp);
    }

    void HealthChange(float newHealth)
    {
        progressBar.SetValue(healthController.CurrentHp / healthController.MaxHp);
        healthText.text = healthController.CurrentHp.ToString("0.0");
    }
}
