﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingWindow : MonoBehaviour
{
    [SerializeField] Canvas canvas;
    [SerializeField] TweenAlphaUI blackoutTween;
    [SerializeField] float fadeTime;

    private void Awake()
    {
        blackoutTween.duration = fadeTime;
        canvas.overrideSorting = true;
        canvas.sortingLayerName = Constants.OVER_EVERYTHING_SORTING_LAYER;
    }

    private void OnEnable()
    {
        blackoutTween.ResetToBeginning();
        blackoutTween.enabled = false;
    }

    public void StartLoading()
    {
        blackoutTween.PlayForward();
    }

    public void FinishedLoading()
    {
        blackoutTween.ResetToEnding();
        blackoutTween.PlayReverse();
    }
}
