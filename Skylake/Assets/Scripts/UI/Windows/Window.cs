﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : MonoBehaviour
{
    [SerializeField] public int layer = 0;
    RectTransform windowRectTransform;

    public void Initialize()
    {
        windowRectTransform = GetComponent<RectTransform>();
        windowRectTransform.localPosition = Vector3.zero;
        windowRectTransform.rotation = Quaternion.identity;
    }

    public virtual void OnOpen()
    {

    }

    public virtual void OnClose()
    {

    }
}
