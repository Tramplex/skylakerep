﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowsHolder : MonoBehaviour
{
    [SerializeField] RectTransform windowsParent;
    [SerializeField] GameObject windowHolderPrefab;
    List<RectTransform> windowsHolders = new List<RectTransform>();
    int windowsHoldersCount = 0;

    private void Awake()
    {
        transform.localPosition = Vector3.zero;
        transform.localScale = Vector3.one;
        AddLayer();
    }

    public RectTransform GetWindowHolder(int layer)
    {
        if(layer >= windowsHoldersCount)
        {
            for(int i = 0; i < layer; i++)
            {
                AddLayer();
            }
            windowsHoldersCount = layer;
        }
        return windowsHolders[layer];
    }

    void AddLayer()
    {
        var windowHolderObject = Instantiate(windowHolderPrefab, windowsParent);
        var windowHolderRect = windowHolderObject.GetComponent<RectTransform>();
        windowHolderRect.localPosition = Vector3.zero;
        windowHolderRect.localRotation = Quaternion.identity;
        windowHolderRect.SetAsLastSibling();
        windowsHolders.Add(windowHolderRect);
    }
}
