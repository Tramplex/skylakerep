﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    public HealthBarType healthBarType;
    [SerializeField] TrackItem trackItem;
    [SerializeField] List<TweenAlphaUI> tweens = new List<TweenAlphaUI>();
    [SerializeField] ProgressBar healthBar;
    [SerializeField] float returnDelay;
    HealthController healthController;
    float maxHealth;
    Coroutine returnCoroutine;

    private void Awake()
    {
        int tweensCount = tweens.Count;
        for (int i = 0; i < tweensCount; i++)
        {
            tweens[i].ResetToBeginning();
            tweens[i].enabled = false;
        }
        EventManager.OnBattleEnd += BattleEnd;
    }

    public void Init(HealthController healthController, Transform healthBarPoint)
    {
        maxHealth = healthController.MaxHp;
        this.healthController = healthController;
        healthController.OnHealthChange += UpdateHealthBar;
        healthController.OnDeath += Deactivate;
        trackItem.StartTracking(healthBarPoint);
        Show();
    }

    void Deactivate()
    {
        trackItem.StopTracking();
        healthController.OnHealthChange -= UpdateHealthBar;
        healthController.OnDeath -= Deactivate;
        healthBar.SetValue(0f);
        Hide();
        returnCoroutine = StartCoroutine(ReturnDelayed());
    }

    IEnumerator ReturnDelayed()
    {
        yield return CoroutineTools.GetYield(returnDelay);
        BattleManager.HealthBarCanvas.ReturnHealthBar(this);
    }

    void BattleEnd()
    {
        EventManager.OnBattleEnd -= BattleEnd;
        CoroutineTools.StopAndNullCoroutine(ref returnCoroutine, this);
        BattleManager.HealthBarCanvas.ReturnHealthBar(this);
    }

    void UpdateHealthBar(float newHealth)
    {
        healthBar.SetValue(newHealth / maxHealth);
    }

    public void Show()
    {
        int tweensCount = tweens.Count;
        for (int i = 0; i < tweensCount; i++)
        {
            tweens[i].PlayForward();
        }
    }

    public void Hide()
    {
        int tweensCount = tweens.Count;
        for (int i = 0; i < tweensCount; i++)
        {
            tweens[i].PlayReverse();
        }
    }
}