﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarCanvas : MonoBehaviour
{
    [SerializeField] Canvas canvas;
    [SerializeField] GameObject smallHealthBarPrefab;
    List<HealthBar> smallHealthBars = new List<HealthBar>();
    Transform myTransform;

    private void Awake()
    {
        myTransform = transform;
    }

    public void Init(Camera mainCamera)
    {
        canvas.worldCamera = mainCamera;
        canvas.planeDistance = 1f;
    }

    public HealthBar GetHealthBar(HealthBarType healthBarType)
    {
        HealthBar resultHealthBar = null;
        switch (healthBarType)
        {
            case HealthBarType.SmallHealthBar:
                if (smallHealthBars.Count == 0)
                {
                    GameObject healthBarObject = Instantiate(smallHealthBarPrefab, myTransform);
                    resultHealthBar = healthBarObject.GetComponent<HealthBar>();
                }
                else
                {
                    resultHealthBar = smallHealthBars[0];
                    smallHealthBars.Remove(resultHealthBar);
                }
                break;
        }
        return resultHealthBar;
    }

    public void ReturnHealthBar(HealthBar returnedHealthBar)
    {
        switch (returnedHealthBar.healthBarType)
        {
            case HealthBarType.SmallHealthBar:
                smallHealthBars.Add(returnedHealthBar);
                break;
        }
    }
}

public enum HealthBarType
{
    NotDefined = 0,
    SmallHealthBar
}