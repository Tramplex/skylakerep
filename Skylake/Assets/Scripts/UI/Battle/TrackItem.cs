﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackItem : MonoBehaviour
{
    [Header("Transform tracking")]
    [SerializeField] Vector2 itemHalfSize;
    [SerializeField] GameObject holder;
    [SerializeField] RectTransform rectTransform;
    protected Transform trackTransform;
    Coroutine moveCoroutine;
    [Space]
    protected float scaleFactor = 1f;
    Camera mainCamera;
    Vector3 positionOnCanvas;
    TrackMode currentTrackMode = TrackMode.Update;
    Vector2 upperRightPoint;
    Vector2 lowerLeftPoint;
    Vector2 farUpperRightPoint;
    Vector2 farLowerLeftPoint;
    float farOffscreenDistance = 1.5f;
    int farOffscreenSkipFrames = 5;
    bool active = false;

    public void StartTracking(Transform trackTransform)
    {
        upperRightPoint = new Vector2(Screen.width + itemHalfSize.x, Screen.height + itemHalfSize.y);
        farUpperRightPoint = new Vector2(Screen.width * farOffscreenDistance + itemHalfSize.x, Screen.height * farOffscreenDistance + itemHalfSize.y);
        lowerLeftPoint = -itemHalfSize;
        farLowerLeftPoint = -itemHalfSize - new Vector2(Screen.width * (farOffscreenDistance - 1), Screen.height * (farOffscreenDistance - 1));
        this.trackTransform = trackTransform;
        moveCoroutine = StartCoroutine(MoveToTarget());
    }

    public void StopTracking()
    {
        CoroutineTools.StopAndNullCoroutine(ref moveCoroutine, this);
        Toggle(false);
        currentTrackMode = TrackMode.Update;
    }

    void Toggle(bool active)
    {
        this.active = active;
        holder.SetActive(active);
        if (active)
        {
            currentTrackMode = TrackMode.Update;
        }
    }

    IEnumerator MoveToTarget()
    {
        positionOnCanvas = Vector3.zero;
        while (true)
        {
            if (mainCamera == null)
            {
                mainCamera = Camera.main;
            }
            switch (currentTrackMode)
            {
                case TrackMode.Update:
                    if (!active)
                    {
                        Toggle(true);
                    }

                    if (mainCamera != null)
                    {
                        positionOnCanvas = mainCamera.WorldToScreenPoint(trackTransform.position);
                        rectTransform.anchoredPosition = positionOnCanvas * scaleFactor;

                        if (positionOnCanvas.x < lowerLeftPoint.x || positionOnCanvas.y < lowerLeftPoint.y
                            || positionOnCanvas.x > upperRightPoint.x || positionOnCanvas.y > upperRightPoint.y)
                        {
                            currentTrackMode = TrackMode.Offscreen;
                        }
                    }
                    break;
                case TrackMode.Offscreen:
                    if (active)
                    {
                        Toggle(false);
                    }

                    if (mainCamera != null)
                    {
                        positionOnCanvas = mainCamera.WorldToScreenPoint(trackTransform.position);

                        if (positionOnCanvas.x < farLowerLeftPoint.x || positionOnCanvas.y < farLowerLeftPoint.y
                            || positionOnCanvas.x > farUpperRightPoint.x || positionOnCanvas.y > farUpperRightPoint.y)
                        {
                            currentTrackMode = TrackMode.FarOffScreen;
                        }
                        else if (positionOnCanvas.x > lowerLeftPoint.x && positionOnCanvas.y > lowerLeftPoint.y
                            && positionOnCanvas.x < upperRightPoint.x && positionOnCanvas.y < upperRightPoint.y)
                        {
                            currentTrackMode = TrackMode.Update;
                        }
                    }
                    break;
                case TrackMode.FarOffScreen:
                    for (int i = 0; i < farOffscreenSkipFrames; i++)
                    {
                        yield return null;
                    }
                    if (mainCamera != null)
                    {
                        positionOnCanvas = mainCamera.WorldToScreenPoint(trackTransform.position);

                        if (positionOnCanvas.x > farLowerLeftPoint.x && positionOnCanvas.y > farLowerLeftPoint.y
                            && positionOnCanvas.x < farUpperRightPoint.x && positionOnCanvas.y < farUpperRightPoint.y)
                        {
                            currentTrackMode = TrackMode.Offscreen;
                        }
                    }
                    break;
            }
            yield return null;
        }
    }

    enum TrackMode
    {
        Update = 0,
        Offscreen = 1,
        FarOffScreen = 2
    }
}
