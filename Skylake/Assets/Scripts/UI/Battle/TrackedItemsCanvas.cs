﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackedItemsCanvas : MonoBehaviour
{
    [SerializeField] Canvas canvas;

    public void Init()
    {
        canvas.worldCamera = Camera.main;
        canvas.planeDistance = 1f;
    }
}
