﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImagesToggler : MonoBehaviour
{
    [SerializeField] float inactiveImageOpacity = 0.4f;
    [SerializeField] List<Image> images = new List<Image>();
    List<float> originalOpacity = new List<float>();
    int imagesCount;
    Color tempColor;

    private void Awake()
    {
        imagesCount = images.Count;
        for(int i = 0; i < imagesCount; i++)
        {
            originalOpacity.Add(images[i].color.a);
        }
    }

    public void Enable()
    {
        for (int i = 0; i < imagesCount; i++)
        {
            tempColor = images[i].color;
            tempColor.a = originalOpacity[i];
            images[i].color = tempColor;
        }
    }

    public void Disable()
    {
        for (int i = 0; i < imagesCount; i++)
        {
            tempColor = images[i].color;
            tempColor.a = originalOpacity[i] * inactiveImageOpacity;
            images[i].color = tempColor;
        }
    }
}
