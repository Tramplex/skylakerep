﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameButton : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] ImagesToggler imagesToggler;
    public event System.Action OnClick;
    public event System.Action OnDown;
    public event System.Action OnUp;
    bool isEnabled;

    public void Enable()
    {
        isEnabled = true;
        imagesToggler.Enable();
    }

    public void Disable()
    {
        isEnabled = false;
        imagesToggler.Disable();
    }

    public void ClearEvents()
    {
        OnClick = null;
        OnDown = null;
        OnUp = null;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(isEnabled) OnClick?.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (isEnabled) OnDown?.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isEnabled) OnUp?.Invoke();
    }
}
