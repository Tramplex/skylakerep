﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopUpTextsController : MonoBehaviour
{
    [SerializeField] Canvas canvas;
    [SerializeField] float cameraMoveEffect;
    [SerializeField] float textsShowSpeed;
    [SerializeField] float textsInitialXSpeed;
    [SerializeField] float textsInitialYSpeed;
    [SerializeField] GameObject popUpTextPrefab;
    [SerializeField] int textsPoolCount;
    [SerializeField] Color startingColor;
    [SerializeField] Color endingColor;
    Camera cam;
    TextMeshProUGUI[] popUpText;
    RectTransform[] popUpTextTransform;
    Vector2[] popUpTextVelocity;
    Color[] popUpTextColor;
    float[] textShowProgress;
    bool[] textActive;
    int popCounter;
    float deltaTime;
    float halfTextsInitialXSpeed;
    Transform myTransform;
    string[] numbersCache = new string[100];
    Vector2 cameraTravel;
    Transform cameraTransform;
    int[] randomDirections = new int[] { -1, 1 };
    TextMeshProUGUI tempText;

    public static PopUpTextsController Instance;

    bool initialized = false;

    private void Awake()
    {
        Instance = this;
    }

    public void Init(Camera mainCamera)
    {
        if (!initialized)
        {
            myTransform = transform;
            halfTextsInitialXSpeed = textsInitialXSpeed * 0.5f;
            PoolTexts();
            CacheNumbers();
        }
        cam = mainCamera;
        cameraTransform = cam.transform;
        canvas.worldCamera = mainCamera;
        canvas.planeDistance = 1f;
    }

    void CacheNumbers()
    {
        int length = numbersCache.Length;
        for (int i = 0; i < length; i++)
        {
            numbersCache[i] = i.ToString();
        }
    }

    void PoolTexts()
    {
        popUpText = new TextMeshProUGUI[textsPoolCount];
        popUpTextTransform = new RectTransform[textsPoolCount];
        popUpTextVelocity = new Vector2[textsPoolCount];
        popUpTextColor = new Color[textsPoolCount];
        textShowProgress = new float[textsPoolCount];
        textActive = new bool[textsPoolCount];
        for (int i = 0; i < textsPoolCount; i++)
        {
            popUpText[i] = Instantiate(popUpTextPrefab, myTransform).GetComponent<TextMeshProUGUI>();
            popUpTextTransform[i] = popUpText[i].GetComponent<RectTransform>();
            popUpTextColor[i] = Color.white;
            popUpText[i].gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (cam != null)
        {
            UpdateTexts();
        }
    }

    public void PopNumberInWorldPosition(int number, Vector3 worldPosition)
    {
        PopNumber(number, cam.WorldToScreenPoint(worldPosition));
    }

    public void PopNumber(int number, Vector2 screenPosition)
    {
        //textActive[popCounter] = true;
        //tempText = popUpText[popCounter];
        //tempText.text = numbersCache[number];
        //tempText.color = popUpTextColor[popCounter] = startingColor;
        //textShowProgress[popCounter] = 1f;
        //popUpTextTransform[popCounter].SetAsLastSibling();
        //popUpTextTransform[popCounter].anchoredPosition = screenPosition;
        //int randomDirection = randomDirections[Random.Range(0, 2)];
        //popUpTextVelocity[popCounter] = new Vector3(Random.Range(halfTextsInitialXSpeed, textsInitialXSpeed) * randomDirection, textsInitialYSpeed);
        //tempText.gameObject.SetActive(true);
        //popCounter++;
        //if (popCounter == textsPoolCount)
        //{
        //    popCounter = 0;
        //}
        PopText(numbersCache[number], screenPosition);
    }

    public void PopText(string text, Vector2 screenPosition)
    {
        textActive[popCounter] = true;
        tempText = popUpText[popCounter];
        tempText.text = text;
        tempText.color = popUpTextColor[popCounter] = startingColor;
        textShowProgress[popCounter] = 1f;
        popUpTextTransform[popCounter].SetAsLastSibling();
        popUpTextTransform[popCounter].anchoredPosition = screenPosition;
        int randomDirection = randomDirections[Random.Range(0, 2)];
        popUpTextVelocity[popCounter] = new Vector3(Random.Range(halfTextsInitialXSpeed, textsInitialXSpeed) * randomDirection * Random.Range(0.5f, 1.5f)
            , textsInitialYSpeed * Random.Range(0.5f, 1.5f));
        tempText.gameObject.SetActive(true);
        popCounter++;
        if(popCounter == textsPoolCount)
        {
            popCounter = 0;
        }
    }

    Vector2 previousCameraPosition;
    Vector2 currentCameraPosition;
    void UpdateTexts()
    {
        currentCameraPosition = new Vector2(cameraTransform.position.x, cameraTransform.position.y);
        cameraTravel = (currentCameraPosition - previousCameraPosition) * cameraMoveEffect;
        previousCameraPosition = currentCameraPosition;
        deltaTime = Time.deltaTime * textsShowSpeed;
        float squaredTextProgress;
        float showProgress;
        for (int i = 0; i < textsPoolCount; i++)
        {
            if (textActive[i])
            {
                showProgress = textShowProgress[i];
                squaredTextProgress = showProgress * showProgress;
                popUpTextTransform[i].anchoredPosition += popUpTextVelocity[i] * squaredTextProgress * deltaTime - cameraTravel;
                popUpText[i].color = popUpTextColor[i];
                if(showProgress < 0.333f)
                {
                    popUpTextColor[i] = UtilityTools.LerpColor(ref endingColor, ref startingColor, showProgress * 3f);
                }
                showProgress -= deltaTime;
                if (showProgress <= 0)
                {
                    textActive[i] = false;
                    popUpText[i].gameObject.SetActive(false);
                }
                else
                {
                    textShowProgress[i] = showProgress;
                }
            }
        }
    }
}
