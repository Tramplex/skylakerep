﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TS.UI
{
	public class GameJoystick : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IDragHandler
	{
		public event System.Action OnTouchDown;
		public event System.Action OnOutputStart;
		public event System.Action OnOutputEnd;
        /// <summary>
        /// Needs to be squared (0.3 deadzone = input 0.09)
        /// </summary>
        [SerializeField] float sqrDeadzone = 0f;
        [SerializeField] bool halfActivationDistance;
        [SerializeField] RectTransform deadzoneImage;
		[SerializeField] RectTransform controlPoint;
        [SerializeField] ImagesToggler imagesToggler;
        public Vector2 output { get; private set; }
        Vector2 joystickSizeDelta;
        Vector2 input;
		Vector2 positionInRect;
		Vector3 joystickPosition;
		Vector2 joystickPositionVector2;
        bool isEnabled = false;
        bool inputCheck = false;
        bool inputReported = false;

        void Awake()
        {
            joystickSizeDelta = GetComponent<RectTransform>().sizeDelta;
            float deadzone = Mathf.Sqrt(sqrDeadzone);
            deadzoneImage.sizeDelta = new Vector2(joystickSizeDelta.x * deadzone, joystickSizeDelta.y * deadzone);
        }

        public void Init()
        {
            joystickPosition = joystickPositionVector2 = GameManager.WindowManager.GetScreenPosition(transform);
        }

        public void Enable()
        {
            isEnabled = true;
            if(imagesToggler != null) imagesToggler.Enable();
        }

        public void Disable()
        {
            isEnabled = false;
            if (imagesToggler != null) imagesToggler.Disable();
        }

        void Update()
        {
            if (!inputCheck || !isEnabled) return;
#if UNITY_EDITOR
            MouseControl();
#endif
            input = new Vector2(positionInRect.x / joystickSizeDelta.x, positionInRect.y / joystickSizeDelta.y) * 2f;
            float inputSqrMagnitude = input.sqrMagnitude;
            if (inputSqrMagnitude < sqrDeadzone)
            {
                output = Vector2.zero;
                if (inputReported)
                {
                    inputReported = false;
                    OnOutputEnd?.Invoke();
                }
            }
            else
            {
                if (halfActivationDistance)
                {
                    output = input * 2f;
                    if (output.sqrMagnitude > 1)
                    {
                        output = output.normalized;
                        input = (inputSqrMagnitude > 1f) ? input.normalized : input;
                    }
                }
                else
                {
                    output = input = (inputSqrMagnitude > 1f) ? input.normalized : input;
                }
                if (!inputReported)
                {
                    inputReported = true;
                    OnOutputStart?.Invoke();
                }
            }
            controlPoint.anchoredPosition = new Vector2(input.x * (joystickSizeDelta.x / 2f), input.y * (joystickSizeDelta.y / 2f));
        }

        void MouseControl()
		{
			positionInRect = Input.mousePosition * GameManager.WindowManager.CanvasScaleFactor - joystickPosition;
		}

        public void OnDrag(PointerEventData eventData)
        {
            positionInRect = eventData.position * GameManager.WindowManager.CanvasScaleFactor - joystickPositionVector2;
        }

		public void OnPointerClick(PointerEventData eventData) { }

		public void OnPointerDown(PointerEventData eventData)
		{
            positionInRect = eventData.position * GameManager.WindowManager.CanvasScaleFactor - joystickPositionVector2;
            inputCheck = true;
            OnTouchDown?.Invoke();
		}

		public void OnPointerUp(PointerEventData eventData)
		{
            output = controlPoint.anchoredPosition = positionInRect = Vector2.zero;
            inputCheck = false;
            inputReported = false;
            OnOutputEnd?.Invoke();
		}

        public void Deactivate()
        {
            inputCheck = false;
            inputReported = false;
            positionInRect = Vector3.zero;
            controlPoint.anchoredPosition = Vector3.zero;
            OnOutputEnd?.Invoke();
        }
	}
}