﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MapGridPart : MonoBehaviour
{
    public Vector2Int gridPartSize;
    public Node[,] gridPart;

    [ContextMenu("Initialize")]
    public void InitializeMapGridPart()
    {
        var tilemapColliders = GetComponentsInChildren<CompositeCollider2D>();
        for(int i = 0; i < tilemapColliders.Length; i++)
        {
            tilemapColliders[i].geometryType = CompositeCollider2D.GeometryType.Polygons;
        }
        Transform myTransform = transform;
        gridPart = new Node[gridPartSize.x, gridPartSize.y];
        for (int x = 0; x < gridPartSize.x; x++)
        {
            for (int y = 0; y < gridPartSize.y; y++)
            {
                Vector3 nodeWorldPosition = new Vector3(myTransform.position.x + Pathfinder.NodeDiameter * x + Pathfinder.mapGridOffset.x
                    , myTransform.position.y + Pathfinder.NodeDiameter * y + Pathfinder.mapGridOffset.x, 0f);
                bool nodeWalkable = Physics2D.OverlapCircle(
                    nodeWorldPosition,
                    Pathfinder.NodeWalkableCheckRadius,
                    Constants.OBSTACLE_LAYER_MASK
                    ) == null;
                gridPart[x, y] = new Node(nodeWalkable, nodeWorldPosition, x, y);
            }
        }
        for (int i = 0; i < tilemapColliders.Length; i++)
        {
            tilemapColliders[i].geometryType = CompositeCollider2D.GeometryType.Outlines;
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (gridPart != null)
        {
            int width = gridPart.GetLength(0);
            int height = gridPart.GetLength(1);
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Gizmos.color = gridPart[x, y].walkable ? Color.green : Color.red;
                    Gizmos.DrawWireSphere(gridPart[x, y].worldPosition, 0.2f);
                }
            }
        }
    }
#endif
}
