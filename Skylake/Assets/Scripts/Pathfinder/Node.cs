﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : IHeapItem<Node>
{
    public bool walkable;
    public Vector3 worldPosition;

    public Node parent;

    public int gridX;
    public int gridY;

    public float gCost;
    public float hCost;
    public float fCost { get { return gCost + hCost; } }
    public int HeapIndex { get; set; }
    public List<Node> neighbors;

    public Node(bool walkable, Vector3 worldPosition, int gridX, int gridY)
    {
        this.walkable = walkable;
        this.worldPosition = worldPosition;
        this.gridX = gridX;
        this.gridY = gridY;
    }

    public void SetNeighbors(List<Node> neighbors)
    {
        this.neighbors = neighbors;
    }

    public int CompareTo(Node item)
    {
        return fCost < item.fCost ? 1 : fCost > item.fCost ? -1 : 0;
    }
}
