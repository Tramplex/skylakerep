﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Pathfinder : MonoBehaviour
{
    public static Pathfinder instance;

    Heap<Node> openSet;
    HashSet<Node> closedSet;

    Node startNode;
    Node targetNode;
    Node closestNode;
    Node currentNode;
    Node neighbor;
    float distanceToNeighbor;
    List<Node> neighbors;
    public void FindPath(PathRequest request, Action<PathResult> callback)
    {
        bool pathSuccess = false;
        startNode = NodeFromWorldPosition(request.pathStart);
        targetNode = NodeFromWorldPosition(request.pathEnd);
        if (!startNode.walkable || !targetNode.walkable)
        {
            callback?.Invoke(new PathResult(new List<Vector3>(), pathSuccess, request.callback));
        }
        closestNode = startNode;
        openSet.Clear();
        closedSet.Clear();
        openSet.Add(startNode);
        while (openSet.Count > 0)
        {
            currentNode = openSet.RemoveFirst();
            closedSet.Add(currentNode);

            if (currentNode == targetNode)
            {
                pathSuccess = true;
                break;
            }

            neighbors = currentNode.neighbors;
            int neighborsCount = neighbors.Count;
            for (int i = 0; i < neighborsCount; i++)
            {
                neighbor = neighbors[i];
                if (!neighbor.walkable || closedSet.Contains(neighbor)) continue;
                distanceToNeighbor = currentNode.gCost + GetDistance(currentNode, neighbor, request.wiggle);
                bool openSetContainsNeighbor = openSet.Contains(neighbor);
                if (distanceToNeighbor < neighbor.gCost || !openSetContainsNeighbor)
                {
                    //if(openSetContainsNeighbor) Debug.Log($"Updated gcost from {neighbor.gCost} to {distanceToNeighbor}");
                    neighbor.gCost = distanceToNeighbor;
                    //Debug.Log($"Updated hcost from {neighbor.hCost} to {GetDistance(neighbors[i], targetNode, wiggle)}");
                    neighbor.hCost = GetDistance(neighbors[i], targetNode, request.wiggle);
                    neighbor.parent = currentNode;
                    if (!openSetContainsNeighbor)
                    {
                        openSet.Add(neighbor);
                    }
                    else
                    {
                        openSet.UpdateItem(neighbor);
                    }
                    if (neighbor.hCost < closestNode.hCost)
                    {
                        closestNode = neighbor;
                    }
                }
            }
        }
        if (pathSuccess)
        {
            callback?.Invoke(new PathResult(RetracePath(startNode, targetNode), pathSuccess, request.callback));
        }
        else
        {
            callback?.Invoke(new PathResult(RetracePath(startNode, closestNode), pathSuccess, request.callback));
        }
    }

    List<Vector3> result = new List<Vector3>();
    List<Vector3> RetracePath(Node startNode, Node endNode)
    {
        result.Clear();
        currentNode = endNode;
        while (currentNode != startNode)
        {
            result.Add(currentNode.worldPosition);
            currentNode = currentNode.parent;
        }
        SimplifyPath(ref result);
        result.Reverse();
        return result;
    }

    List<int> removeIndexes = new List<int>();
    void SimplifyPath(ref List<Vector3> path)
    {
        removeIndexes.Clear();
        Vector2 oldDirection = Vector2.zero;
        int pathLength = path.Count;
        for (int i = 1; i < pathLength; i++)
        {
            Vector2 newDirection = new Vector2(path[i - 1].x - path[i].x, path[i - 1].y - path[i].y);
            if (newDirection == oldDirection)
            {
                removeIndexes.Add(i);
            }
            else
            {
                oldDirection = newDirection;
            }
        }
        int removeIndexesCount = removeIndexes.Count;
        for (int i = removeIndexesCount - 1; i >= 0; i--)
        {
            path.RemoveAt(removeIndexes[i]);
        }
    }

    float randomDiagonalWeight;
    float GetDistance(Node nodeA, Node nodeB, float wiggle)
    {
        int dX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dY = Mathf.Abs(nodeA.gridY - nodeB.gridY);
        randomDiagonalWeight = UnityEngine.Random.Range(1.4f, 1.4f + wiggle);
        return (dX > dY) ? randomDiagonalWeight * dY + 1f * (dX - dY) : randomDiagonalWeight * dX + 1f * (dY - dX);
    }
}
