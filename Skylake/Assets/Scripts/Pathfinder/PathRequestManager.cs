﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

public class PathRequestManager : MonoBehaviour
{
    static Queue<PathResult> pathResultsQueue = new Queue<PathResult>();

    void Update()
    {
        if(pathResultsQueue.Count > 0)
        {
            int resultsCount = pathResultsQueue.Count;
            lock (pathResultsQueue)
            {
                for (int i = 0; i < resultsCount; i++)
                {
                    var result = pathResultsQueue.Dequeue();
                    result.callback?.Invoke(result.path, result.success);
                }
            }
        }
    }

    public static void RequestPath(PathRequest request)
    {
        ThreadStart threadStart = delegate
        {
            Pathfinder.instance.FindPath(request, FinishedProcessingPath);
        };
        threadStart.Invoke();
    }

    static void FinishedProcessingPath(PathResult result)
    {
        lock (pathResultsQueue)
        {
            pathResultsQueue.Enqueue(result);
        }
    }
}

public class PathResult
{
    public List<Vector3> path;
    public bool success;
    public Action<List<Vector3>, bool> callback;

    public PathResult(List<Vector3> path, bool success, Action<List<Vector3>, bool> callback)
    {
        this.path = path;
        this.success = success;
        this.callback = callback;
    }
}

public class PathRequest
{
    public Vector3 pathStart;
    public Vector3 pathEnd;
    public Action<List<Vector3>, bool> callback;
    public float wiggle;

    public PathRequest(Vector3 pathStart, Vector3 pathEnd, Action<List<Vector3>, bool> callback, float wiggle)
    {
        this.pathStart = pathStart;
        this.pathEnd = pathEnd;
        this.callback = callback;
        this.wiggle = wiggle;
    }
}
