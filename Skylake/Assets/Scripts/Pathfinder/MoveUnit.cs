﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUnit : MonoBehaviour
{
    [SerializeField] float waypointReachedThreshold;
    [SerializeField] float maxMoveSpeed;
    [SerializeField] float rotationAcceleration;
    [SerializeField] float maxRotationSpeed;
    [SerializeField] float wiggle;
    Transform myTransform;
    List<Vector3> path;
    int targetIndex;
    Coroutine followPathCoroutine;
    float currentAngle, targetAngle;
    float deltaTime;
    float currentSpeed;
    float currentRotationSpeed;

    private void Start()
    {
        myTransform = transform;
    }

    public void MoveTo(Vector3 position)
    {
        PathRequestManager.RequestPath(new PathRequest(transform.position, position, PathFound, wiggle));
    }

    void PathFound(List<Vector3> path, bool success)
    {
        if (success)
        {
            this.path = path;
            CoroutineTools.StopAndNullCoroutine(ref followPathCoroutine, this);
            followPathCoroutine = StartCoroutine(FollowPath());
        }
    }

    IEnumerator FollowPath()
    {
        targetIndex = 0;
        Vector3 currentWaypoint = path[targetIndex];
        int pathLength = path.Count;
        bool pointReached = false;
        bool rotationReached = false;
        while (true)
        {
            deltaTime = Time.deltaTime;
            currentAngle = myTransform.rotation.eulerAngles.z;
            if (Mathf.Abs(myTransform.position.x - currentWaypoint.x) < waypointReachedThreshold
                && Mathf.Abs(myTransform.position.y - currentWaypoint.y) < waypointReachedThreshold)
            {
                targetIndex++;
                if (targetIndex >= pathLength)
                {
                    pointReached = true;
                }
                if (!pointReached)
                {
                    currentWaypoint = path[targetIndex];
                }
            }
            targetAngle = Vector2.SignedAngle(Vector2.up, currentWaypoint - myTransform.position);
            if (targetAngle < 0) targetAngle += 360f;
            currentRotationSpeed = (Mathf.Abs(currentAngle - targetAngle) > 0) ? 
                Mathf.MoveTowards(currentRotationSpeed, maxRotationSpeed, rotationAcceleration * deltaTime) 
                : Mathf.MoveTowards(currentRotationSpeed, 0, rotationAcceleration * deltaTime);
            currentSpeed = maxMoveSpeed * Mathf.Clamp01(Mathf.InverseLerp(90f, 0f, Mathf.Abs(currentAngle - targetAngle)));
            myTransform.position = Vector3.MoveTowards(myTransform.position, currentWaypoint, currentSpeed * deltaTime);
            currentAngle = Mathf.MoveTowardsAngle(currentAngle, targetAngle, currentRotationSpeed * deltaTime);
            myTransform.rotation = Quaternion.Euler(0f, 0f, currentAngle);
            if (targetAngle - currentAngle < 1)
            {
                rotationReached = true;
            }
            if (pointReached && rotationReached)
            {
                yield break;
            }
            yield return null;
        }
    }

    private void OnDrawGizmos()
    {
        if (path != null && path.Count > 0)
        {
            Gizmos.color = Color.red;
            for (int i = 0; i < path.Count; i++)
            {
                Gizmos.DrawWireSphere(path[i], 0.1f);
                if (i > 0)
                {
                    Gizmos.DrawLine(path[i - 1], path[i]);
                }
            }
        }
    }
}
