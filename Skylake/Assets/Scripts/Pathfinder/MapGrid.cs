﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Pathfinder : MonoBehaviour
{
    [SerializeField] bool drawGizmoGrid;
    [SerializeField] bool debugWithUnit;
    [SerializeField] Transform debugPlane;
    [SerializeField] MoveUnit debugUnit;
    [SerializeField] LayerMask debugLayer;
    Vector2 gridWorldPosition;
    Node[,] grid;
    public static float NodeRadius = 0.2f;
    public static Vector2 mapGridOffset = new Vector2(NodeRadius, NodeRadius);
    public static float NodeWalkableCheckRadius = 0.35f;
    public static float NodeDiameter = NodeRadius * 2f;
    public Vector2 gridWorldSize;
    public LayerMask unwalkableMask;
    int gridSizeX, gridSizeY;
    public int MaxSize { get { return gridSizeX * gridSizeY; } }

    private void Awake()
    {
        instance = this;
        gridWorldPosition = transform.position;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / NodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / NodeDiameter);
        openSet = new Heap<Node>(MaxSize);
        closedSet = new HashSet<Node>();
        if(debugUnit != null) debugUnit.gameObject.SetActive(debugWithUnit);
        if (debugPlane != null)
        {
            debugPlane.gameObject.SetActive(true);
            debugPlane.localScale = new Vector3(gridWorldSize.x, gridWorldSize.y, 1f);
        }
        CreateGrid();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 20f, debugLayer))
            {
                //Debug.Log($"{hit.point.x} {NodeFromWorldPosition(hit.point).gridX} {NodeFromWorldPosition(hit.point).worldPosition.x}");
                if (debugWithUnit)
                {
                    debugUnit.MoveTo(hit.point);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            for (int i = 0; i < 50; i++)
            {
                PathRequestManager.RequestPath(new PathRequest(new Vector3(5f, 0f, 0f), new Vector3(-5f, 3.5f, 0f), null, 0));
            }
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            PathRequestManager.RequestPath(new PathRequest(new Vector3(5f, 0f, 0f), new Vector3(-5f, 3.5f, 0f), null, 0));
        }
    }

    public Node NodeFromWorldPosition(Vector3 worldPosition)
    {
        int x = Mathf.Clamp(Mathf.RoundToInt((worldPosition.x - gridWorldPosition.x + gridWorldSize.x * 0.5f - mapGridOffset.x) / NodeDiameter), 0, gridSizeX);
        int y = Mathf.Clamp(Mathf.RoundToInt((worldPosition.y - gridWorldPosition.y + gridWorldSize.y * 0.5f - mapGridOffset.y) / NodeDiameter), 0, gridSizeY);
        if(x >= grid.GetLength(0) || y >= grid.GetLength(1))
        {
            Debug.Log($"Out of borders {worldPosition}");
        }
        return grid[x,y];
    }

    public void CacheNodeNeighbors(Node node)
    {
        List<Node> result = new List<Node>();
        int startX = node.gridX > 0 ? -1 : 0;
        int endX = node.gridX < gridSizeX - 1 ? 1 : 0;
        int startY = node.gridY > 0 ? -1 : 0;
        int endY = node.gridY < gridSizeY - 1 ? 1 : 0;
        for (int x = startX; x <= endX; x++)
        {
            for (int y = startY; y <= endY; y++)
            {
                if (x == 0 && y == 0) continue;
                result.Add(grid[node.gridX + x, node.gridY + y]);
            }
        }
        node.SetNeighbors(result);
    }

    void CreateGrid()
    {
        var tilemapColliders = FindObjectsOfType<CompositeCollider2D>();
        for (int i = 0; i < tilemapColliders.Length; i++)
        {
            tilemapColliders[i].geometryType = CompositeCollider2D.GeometryType.Polygons;
        }
        grid = new Node[gridSizeX, gridSizeY];
        Vector2 worldBottomLeft = gridWorldPosition - gridWorldSize * 0.5f + mapGridOffset;
        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector2 worldPosition = worldBottomLeft + new Vector2(x * NodeDiameter, y * NodeDiameter);
                bool nodeWalkable = Physics2D.OverlapCircle(
                    worldPosition,
                    NodeWalkableCheckRadius,
                    Constants.OBSTACLE_LAYER_MASK
                    ) == null;
                var node = new Node(nodeWalkable, worldPosition, x, y);
                grid[x, y] = node;
            }
        }
        for (int i = 0; i < tilemapColliders.Length; i++)
        {
            tilemapColliders[i].geometryType = CompositeCollider2D.GeometryType.Outlines;
        }
        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                CacheNodeNeighbors(grid[x, y]);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, gridWorldSize);
        if(drawGizmoGrid && grid != null)
        {
            Vector3 cubeSize = Vector3.one * 0.02f;
            for(int x = 0; x < grid.GetLength(0); x++)
            {
                for (int y = 0; y < grid.GetLength(1); y++)
                {
                    Gizmos.color = grid[x,y].walkable ? Color.green : Color.red;
                    Gizmos.DrawWireCube(grid[x, y].worldPosition, cubeSize);
                }
            }
        }
    }
}
