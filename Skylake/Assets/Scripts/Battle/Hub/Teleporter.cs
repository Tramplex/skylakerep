﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField] SceneNamesEnum sceneToLoad;

    public void OnTriggerEnter2D(Collider2D collider)
    {
        var player = collider.GetComponentInParent<Player>();
        if(player != null)
        {
            GameManager.Instance.LoadScene(sceneToLoad.ToString());
        }
    }
}
