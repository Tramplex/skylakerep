﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    [SerializeField] PrefabsDatabase prefabsDatabase;
	Dictionary<int, List<PooledObject>> objectsPool = new Dictionary<int, List<PooledObject>>();
	const string cloneString = "(Clone)";
    PooledObject result;
    PooledObject returnedPooledObject;
    Transform poolTransform;
    System.Action OnPoolClear;

	public void Init()
	{
		EventManager.OnSceneLoaded += CreatePoolTransform;
	}

	void CreatePoolTransform(SceneLoadedEventData eventData)
    {
        poolTransform = new GameObject(Constants.POOL_TRANSFORM_NAME).transform;
        poolTransform.position = Vector3.zero;
    }

    public GameObject Instantiate(int id)
	{
		GameObject result = Instantiate(prefabsDatabase.GetPrefab(id), poolTransform);
		result.name = result.name.Replace(cloneString, string.Empty);
		return result;
	}

	public GameObject PullObject(IDEnum oblectEnumId, Vector3 position, Quaternion rotation)
	{
        int objectId = (int)oblectEnumId;
		if (!objectsPool.ContainsKey(objectId))
		{
			objectsPool.Add(objectId, new List<PooledObject>());
		}
		if (objectsPool[objectId].Count == 0)
		{
			GameObject newObject = Instantiate(objectId);
			PooledObject newPooledObject = newObject.GetComponent<PooledObject>();
			if (newPooledObject != null)
			{
				newPooledObject.Init(this, ref OnPoolClear, objectId);
				objectsPool[objectId].Add(newPooledObject);
			}
			else
			{
				Debug.LogError("Object " + newObject.name + " is not PooledObject");
				Destroy(newObject);
			}
		}
		result = objectsPool[objectId][0];
		objectsPool[objectId].Remove(result);
        result.transform.position = position;
        result.transform.rotation = rotation;
		result.OnPull();
		return result.gameObject;
	}

	public void PushObject(GameObject pushedObject)
	{
		returnedPooledObject = pushedObject.GetComponent<PooledObject>();
		if (returnedPooledObject != null)
		{
			returnedPooledObject.OnPush();
			if (!objectsPool.ContainsKey(returnedPooledObject.id))
			{
				objectsPool.Add(returnedPooledObject.id, new List<PooledObject>());
			}
			objectsPool[returnedPooledObject.id].Add(returnedPooledObject);
		}
		else
		{
			Debug.LogError(pushedObject.name + " is not PooledObject!");
		}
	}

    public void PushObject(PooledObject pushedObject)
    {
        if (pushedObject != null)
        {
            pushedObject.OnPush();
            if (!objectsPool.ContainsKey(pushedObject.id))
            {
                objectsPool.Add(pushedObject.id, new List<PooledObject>());
            }
            objectsPool[pushedObject.id].Add(pushedObject);
        }
    }

    public void ResetPool()
    {
        OnPoolClear?.Invoke();
        OnPoolClear = null;
        objectsPool.Clear();
        result = null;
    }
}
