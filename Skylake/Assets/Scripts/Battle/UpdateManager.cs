﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateManager : MonoBehaviour
{
    List<IUpdatable> updatedObjects = new List<IUpdatable>();
    List<IFixedUpdatable> fixedUpdatedObjects = new List<IFixedUpdatable>();

    public void SubscribeForUpdate(IUpdatable updatableObject)
    {
        if (!updatableObject.SubscribedForUpdates)
        {
            updatableObject.SubscribedForUpdates = true;
            updatedObjects.Add(updatableObject);
        }
    }

    public void UnsubscribeFromUpdate(IUpdatable updatableObject)
    {
        if (updatableObject.SubscribedForUpdates)
        {
            updatableObject.SubscribedForUpdates = false;
            updatedObjects.Remove(updatableObject);
        }
    }

    public void SubscribeForUpdateFixed(IFixedUpdatable updatableFixedObject)
    {
        if (!updatableFixedObject.SubscribedForFixedUpdates)
        {
            updatableFixedObject.SubscribedForFixedUpdates = true;
            fixedUpdatedObjects.Add(updatableFixedObject);
        }
    }

    public void UnsubscribeFromUpdateFixed(IFixedUpdatable updatableFixedObject)
    {
        if (updatableFixedObject.SubscribedForFixedUpdates)
        {
            updatableFixedObject.SubscribedForFixedUpdates = false;
            fixedUpdatedObjects.Remove(updatableFixedObject);
        }
    }

    private void Update()
    {
        for (int i = 0; i < updatedObjects.Count; i++)
        {
            updatedObjects[i].OnUpdate();
        }
    }

    private void FixedUpdate()
    {
        for (int i = 0; i < fixedUpdatedObjects.Count; i++)
        {
            fixedUpdatedObjects[i].OnFixedUpdate();
        }
    }
}

public interface IUpdatable
{
    void OnUpdate();
    bool SubscribedForUpdates { get; set; }
}

public interface IFixedUpdatable
{
    void OnFixedUpdate();
    bool SubscribedForFixedUpdates { get; set; }
}
