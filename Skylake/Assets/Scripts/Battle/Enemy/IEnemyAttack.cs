﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyAttack
{
    EnemyAttackType attackType { get; }
    event System.Action OnAttackFinished;
    void Init(EnemyStateData stateData);
    void StartAttack(Transform attackTarget);
    void CancelAttack();
}
