﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyBehaviour
{
    EnemyState BehaviourType { get; }
    void Init(Hashtable parameters
        , System.Action<Transform> ChaseOrderAction
        , System.Action<Vector3> MoveOrderAction
        , System.Action StopMoveOrderAction
        , System.Action<Transform, EnemyAttackType> AttackOrderAction
        , System.Action StopAttackOrderAction
        , System.Action<Transform> LookAtOrderAction
        , System.Action StopLookAtOrderAction
        , System.Action<AnimationEnum> PlayAnimationAction);
    void Activate();
    void Deactivate();
}
