﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyViewer : MonoBehaviour
{
    [SerializeField] Animator anim;
    List<int> allTriggersHashes = new List<int>();
    int triggersCount;

    public void Init(ref System.Action<AnimationEnum> OnPlayAnimationAction)
    {
        OnPlayAnimationAction += PlayAnimation;
        allTriggersHashes.Add(Constants.GetAnimationHash(AnimationEnum.Idle));
        allTriggersHashes.Add(Constants.GetAnimationHash(AnimationEnum.Attack));
        allTriggersHashes.Add(Constants.GetAnimationHash(AnimationEnum.Move));
        triggersCount = 3;
    }

    public void PlayAnimation(AnimationEnum animationType)
    {
        //Debug.Log($"Play animation {animationType}");
        RestTriggers();
        anim.SetTrigger(Constants.GetAnimationHash(animationType));
    }

    void RestTriggers()
    {
        for(int i = 0; i < triggersCount; i++)
        {
            anim.ResetTrigger(allTriggersHashes[i]);
        }
    }
}
