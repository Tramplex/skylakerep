﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent (typeof(Rigidbody2D))]
public class EnemyMove : MonoBehaviour, IUpdatable, IFixedUpdatable
{
    public bool SubscribedForUpdates { get; set; }
    public bool SubscribedForFixedUpdates { get; set; }
    float waypointReachedThreshold = 0.02f;
    float maxRotationSpeed;
    List<Vector3> path;
    int targetIndex;
    float maxMoveSpeed;
    float moveWiggle;
    Transform myTransform;
    Rigidbody2D myRigidbody;
    Vector2 myPosition;
    float currentAngle, targetAngle;
    float deltaTime;
    float currentSpeed;
    float rotationAcceleration;
    float currentRotationSpeed;
    Vector3 currentWaypoint;
    Vector3 destination;
    Transform chaseTarget;
    int pathLength;
    bool pointReached;
    bool rotationReached;
    bool subscribedForUpdate = false;
    EnemyStateData stateData;
    Transform lookTarget;
    public MoveType currentMoveType;
    public WorkMode currentWorkMode;
    System.Action<AnimationEnum> PlayAnimationAction;
    float colliderRadius;

    public void Init(EnemyStateData stateData
        , ref System.Action<Transform> OnChaseAction
        , ref System.Action<Vector3> OnMoveAction
        , ref System.Action OnStopMoveAction
        , ref System.Action<Transform> OnLookAtAction
        , ref System.Action OnStopLookAtAction
        , ref System.Action<AnimationEnum> PlayAnimationOrderAction)
    {
        this.stateData = stateData;
        stateData.OnStateChange += StateChanged;
        OnChaseAction += Chase;
        OnMoveAction += Move;
        OnStopMoveAction += Stop;
        OnLookAtAction += LookAt;
        OnStopLookAtAction += StopLookAt;
        PlayAnimationAction = PlayAnimationOrderAction;
        rotationAcceleration = stateData.enemyData.RotationAcceleration;
        maxRotationSpeed = stateData.enemyData.RotationSpeed;
        moveWiggle = stateData.enemyData.MoveWiggle;
        myTransform = transform;
        myPosition = myTransform.position;
        myRigidbody = GetComponent<Rigidbody2D>();
        colliderRadius = GetComponent<CircleCollider2D>().radius;
    }

    public void Deactivate()
    {
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
        GameManager.UpdateManager.UnsubscribeFromUpdateFixed(this);
    }

    public void LookAt(Transform lookTarget)
    {
        StopMove();
        this.lookTarget = lookTarget;
        currentWorkMode = WorkMode.LookAt;
        GameManager.UpdateManager.SubscribeForUpdate(this);
        GameManager.UpdateManager.SubscribeForUpdateFixed(this);
    }

    public void StopLookAt()
    {
        lookTarget = null;
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
        GameManager.UpdateManager.UnsubscribeFromUpdateFixed(this);
    }

    public void Chase(Transform chaseTarget)
    {
        StopLookAt();
        currentMoveType = MoveType.Chase;
        this.chaseTarget = chaseTarget;
        stateData.isMoving = true;
        if (IsPointInView(chaseTarget.position))
        {
            destination = chaseTarget.position;
            pointReached = false;
            rotationReached = false;
            currentWorkMode = WorkMode.MoveDirectly;
            PlayAnimationAction?.Invoke(AnimationEnum.Move);
            GameManager.UpdateManager.SubscribeForUpdate(this);
            GameManager.UpdateManager.SubscribeForUpdateFixed(this);
        }
        else
        {
            currentWorkMode = WorkMode.MoveByPathfinder;
            PathRequestManager.RequestPath(new PathRequest(myTransform.position, chaseTarget.position, PathFound, moveWiggle));
        }
    }

    public void Move(Vector3 targetPosition)
    {
        StopLookAt();
        stateData.isMoving = true;
        currentMoveType = MoveType.Move;
        if (IsPointInView(targetPosition))
        {
            destination = targetPosition;
            pointReached = false;
            rotationReached = false;
            currentWorkMode = WorkMode.MoveDirectly;
            PlayAnimationAction?.Invoke(AnimationEnum.Move);
            GameManager.UpdateManager.SubscribeForUpdate(this);
            GameManager.UpdateManager.SubscribeForUpdateFixed(this);
        }
        else
        {
            currentWorkMode = WorkMode.MoveByPathfinder;
            PathRequestManager.RequestPath(new PathRequest(myTransform.position, targetPosition, PathFound, moveWiggle));
        }
    }

    public void Stop()
    {
        PlayAnimationAction?.Invoke(AnimationEnum.Idle);
        StopMove();
    }

    void StopMove()
    {
        stateData.isMoving = false;
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
        GameManager.UpdateManager.UnsubscribeFromUpdateFixed(this);
    }

    bool IsPointInView(Vector2 destinationPosition)
    {
        bool result = true;
        Vector2 direction = destinationPosition - myPosition;
        if (Physics2D.CircleCast(myPosition, colliderRadius, direction, direction.magnitude, Constants.OBSTACLE_LAYER_MASK))
        {
            result = false;
        }
        //Debug.DrawRay(myPosition, direction, result ? Color.green : Color.red, 1f);
        return result;
    }

    void StateChanged(EnemyState newState)
    {
        switch (newState)
        {
            case EnemyState.Attacking: 
                maxMoveSpeed = stateData.enemyData.AttackMoveSpeed;
                break;
            default:
                maxMoveSpeed = stateData.enemyData.IdleMoveSpeed;
                break;
        }
    }

    void PathFound(List<Vector3> path, bool success)
    {
        if (path.Count > 0)
        {
            targetIndex = UtilityTools.FindClosestNode(path, myTransform.position);
            FollowNewPath(path);
            pointReached = false;
            rotationReached = false;
            PlayAnimationAction?.Invoke(AnimationEnum.Move);
            GameManager.UpdateManager.SubscribeForUpdate(this);
            GameManager.UpdateManager.SubscribeForUpdateFixed(this);
        }
    }

    void FollowNewPath(List<Vector3> path)
    {
        this.path = path;
        currentWaypoint = path[targetIndex];
        pathLength = path.Count;
        destination = path[pathLength - 1];
    }

    float checkDestinationInViewTimer = 0f;
    float findNewPathTimer = 0f;
    public void OnUpdate()
    {
        myPosition = myTransform.position;
        deltaTime = Time.deltaTime;
        currentAngle = myTransform.rotation.eulerAngles.z;
        bool checkDestinationInView = true;
        switch (currentWorkMode)
        {
            case WorkMode.MoveByPathfinder:
                findNewPathTimer += deltaTime;
                if(findNewPathTimer > Constants.FIND_NEW_PATH_COOLDOWN)
                {
                    findNewPathTimer = 0f;
                    switch (currentMoveType)
                    {
                        case MoveType.Chase:
                            PathRequestManager.RequestPath(new PathRequest(myTransform.position, chaseTarget.position, PathFound, moveWiggle));
                            break;
                        case MoveType.Move: break;
                    }
                }
                break;
            case WorkMode.MoveDirectly:
                break;
            case WorkMode.LookAt:
                checkDestinationInView = false;
                break;
        }
        if (checkDestinationInView)
        {
            checkDestinationInViewTimer += deltaTime;
            if (checkDestinationInViewTimer > Constants.CHECK_DESTINATION_IN_VIEW_COOLDOWN)
            {
                checkDestinationInViewTimer = 0;
                bool targetPointInView = false;
                switch (currentMoveType)
                {
                    case MoveType.Chase:
                        targetPointInView = IsPointInView(chaseTarget.position);
                        break;
                    case MoveType.Move:
                        targetPointInView = IsPointInView(destination);
                        break;
                }
                SetWorkMode(targetPointInView ? WorkMode.MoveDirectly : WorkMode.MoveByPathfinder);
            }
        }
    }

    public void OnFixedUpdate()
    {
        switch (currentWorkMode)
        {
            case WorkMode.MoveDirectly:
                switch (currentMoveType)
                {
                    case MoveType.Chase:
                        Look(chaseTarget.position - myTransform.position);
                        MoveTo(chaseTarget.position);
                        if ((myTransform.position - chaseTarget.position).sqrMagnitude < stateData.enemyData.ReachDistance)
                        {
                            pointReached = true;
                        }
                        break;
                    case MoveType.Move:
                        Look(destination - myTransform.position);
                        MoveTo(destination);
                        if ((myTransform.position - destination).sqrMagnitude < stateData.enemyData.ReachDistance)
                        {
                            pointReached = true;
                        }
                        break;
                }
                if (pointReached && rotationReached)
                {
                    Stop();
                }
                break;
            case WorkMode.MoveByPathfinder:
                Look(currentWaypoint - myTransform.position);
                MoveTo(currentWaypoint);
                if (Mathf.Abs(myTransform.position.x - currentWaypoint.x) < waypointReachedThreshold
                           && Mathf.Abs(myTransform.position.y - currentWaypoint.y) < waypointReachedThreshold)
                {
                    targetIndex++;
                    if (targetIndex >= pathLength)
                    {
                        pointReached = true;
                    }
                    if (!pointReached)
                    {
                        currentWaypoint = path[targetIndex];
                    }
                }
                if (pointReached && rotationReached)
                {
                    Stop();
                }
                break;
            case WorkMode.LookAt:
                Look(lookTarget.position - myTransform.position);
                break;
        }
    }

    //void OnDrawGizmos()
    //{
    //    if (!Application.isPlaying) return;
    //    if(path != null)
    //    {
    //        Gizmos.color = Color.black;
    //        for(int i = 0; i < path.Length; i++)
    //        {
    //            Gizmos.DrawWireSphere(path[i], 0.1f);
    //        }
    //    }
    //    Gizmos.color = Color.cyan;
    //    switch (currentWorkMode)
    //    {
    //        case WorkMode.MoveDirectly:
    //            switch (currentMoveType)
    //            {
    //                case MoveType.Chase:
    //                    Gizmos.DrawLine(myTransform.position, chaseTarget.position);
    //                    break;
    //                case MoveType.Move:
    //                    Gizmos.DrawLine(myTransform.position, destination);
    //                    break;
    //            }
    //            break;
    //        case WorkMode.MoveByPathfinder:
    //            Gizmos.DrawLine(myTransform.position, currentWaypoint);
    //            break;
    //    }
    //}

    void SetWorkMode(WorkMode newWorkMode)
    {
        if(currentWorkMode != newWorkMode)
        {
            if (newWorkMode == WorkMode.MoveByPathfinder)
            {
                switch (currentMoveType)
                {
                    case MoveType.Chase: currentWaypoint = chaseTarget.position; break;
                    case MoveType.Move: currentWaypoint = destination; break;
                }
                findNewPathTimer = Constants.FIND_NEW_PATH_COOLDOWN;
            }
            currentWorkMode = newWorkMode;
            //Debug.Log($"Changed work mode to {newWorkMode}");
        }
    }

    void Look(Vector3 lookDirection)
    {
        targetAngle = Vector2.SignedAngle(Vector2.up, lookDirection);
        if (targetAngle < 0) targetAngle += 360f;
        currentRotationSpeed = (Mathf.Abs(currentAngle - targetAngle) > 0) ?
            Mathf.MoveTowards(currentRotationSpeed, maxRotationSpeed, rotationAcceleration * deltaTime)
            : Mathf.MoveTowards(currentRotationSpeed, 0, rotationAcceleration * deltaTime);
        currentAngle = Mathf.MoveTowardsAngle(currentAngle, targetAngle, currentRotationSpeed * deltaTime);
        //myTransform.rotation = Quaternion.Euler(0f, 0f, currentAngle);
        myRigidbody.MoveRotation(currentAngle);
        if (targetAngle - currentAngle < 1)
        {
            rotationReached = true;
        }
    }

    void MoveTo(Vector3 position)
    {
        currentSpeed = maxMoveSpeed * Mathf.Clamp01(Mathf.InverseLerp(90f, 20f, Mathf.Abs(currentAngle - targetAngle)));
        myRigidbody.MovePosition(Vector3.MoveTowards(myTransform.position, position, currentSpeed * Time.fixedDeltaTime));
    }
}

public enum WorkMode
{
    MoveByPathfinder,
    MoveDirectly,
    LookAt
}

public enum MoveType
{
    Move,
    Chase,
}
