﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackController : MonoBehaviour
{
    Dictionary<int, IEnemyAttack> attacks = new Dictionary<int, IEnemyAttack>();
    int defaultAttack;
    int currentActiveAttack;
    EnemyStateData stateData;
    System.Action<AnimationEnum> PlayAnimationOrder;

    public void Init(EnemyStateData stateData, ref System.Action<Transform, EnemyAttackType> OnStartAttackAction
        , ref System.Action OnStopAttackAction, ref System.Action<AnimationEnum> PlayAnimationOrderAction)
    {
        this.stateData = stateData;
        OnStartAttackAction += AttackTarget;
        OnStopAttackAction += CancelAttack;
        PlayAnimationOrder = PlayAnimationOrderAction;
        attacks.Clear();
        var presentAttacks = GetComponents<IEnemyAttack>();
        int presentAttacksCount = presentAttacks.Length;
        for (int i = 0;i < presentAttacksCount; i++)
        {
            if (i == 0) defaultAttack = (int)presentAttacks[i].attackType;
            attacks.Add((int)presentAttacks[i].attackType, presentAttacks[i]);
            presentAttacks[i].Init(stateData);
            presentAttacks[i].OnAttackFinished += AttackFinished;
        }
    }

    public void Deactivate()
    {
        CancelAttack();
    }

    public void AttackTarget(Transform attackTarget, EnemyAttackType attackType)
    {
        int attackTypeInt = (int)attackType;
        if (!attacks.ContainsKey(attackTypeInt))
        {
            attackTypeInt = defaultAttack;
            Debug.Log($"Attack type {attackType} not present on enemy {gameObject.name}");
        }
        PlayAnimationOrder.Invoke(AnimationEnum.Attack);
        currentActiveAttack = attackTypeInt;
        stateData.isAttacking = true;
        attacks[attackTypeInt].StartAttack(attackTarget);
    }

    void CancelAttack()
    {
        if (currentActiveAttack > 0)
        {
            attacks[currentActiveAttack].CancelAttack();
        }
        stateData.isAttacking = false;
    }

    void AttackFinished()
    {
        currentActiveAttack = -1;
        stateData.isAttacking = false;
    }
}
