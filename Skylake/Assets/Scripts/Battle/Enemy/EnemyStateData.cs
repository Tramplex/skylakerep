﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateData : MonoBehaviour
{
    public UnitData enemyData;
    public bool isMoving = false;
    public bool isAttacking = false;
    public event System.Action<EnemyState> OnStateChange;
    public EnemyState currentState;

    public void Init()
    {
        ChangeState(enemyData.InitialState);
    }

    public void ChangeState(EnemyState newState)
    {
        currentState = newState;
        OnStateChange?.Invoke(newState);
    }
}
