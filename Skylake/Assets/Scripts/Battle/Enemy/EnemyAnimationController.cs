﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationController : MonoBehaviour
{
    Animator anim;
    int activeTrigger;

    public void Init(Animator anim, ref System.Action<AnimationEnum> OnPlayAnimation)
    {
        this.anim = anim;
        OnPlayAnimation += SetAnimation;
    }

    public void SetAnimation(AnimationEnum animation)
    {
        ResetAnimator();
        activeTrigger = Constants.GetAnimationHash(animation);
        anim.SetTrigger(activeTrigger);
    }

    void ResetAnimator()
    {
        if(activeTrigger > 0)
        {
            anim.ResetTrigger(activeTrigger);
            activeTrigger = 0;
        }
    }
}
