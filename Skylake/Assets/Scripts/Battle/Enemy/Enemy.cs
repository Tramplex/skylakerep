﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class Enemy : MonoBehaviour , IUpdatable
{
    public bool SubscribedForUpdates { get; set; }
    [SerializeField] EnemyStateData stateData;
    [SerializeField] EnemyMove moveController;
    [SerializeField] EnemyViewer enemyViewer;
    [SerializeField] EnemyAttackController attackController;
    [SerializeField] HealthController healthController;
    [SerializeField] Vector3 deployEffectOffset;
    [SerializeField] IDEnum deployEffectId;
    public event System.Action<Transform> OnLookAt;
    public event System.Action OnStopLookAt;
    public event System.Action<Transform> OnChaseTarget;
    public event System.Action<Vector3> OnMoveToPosition;
    public event System.Action OnStopMove;
    public event System.Action<Transform, EnemyAttackType> OnStartAttack;
    public event System.Action OnStopAttack;
    public event System.Action<AnimationEnum> OnPlayAnimation;
    public System.Action<Enemy> OnDead;
    Coroutine deployCoroutine;
    Dictionary<int, IEnemyBehaviour> behaviours = new Dictionary<int, IEnemyBehaviour>();
    IEnemyBehaviour currentBehaviour;
    Transform playerTransform;
    public Transform myTransform;
    bool initializedComponents = false;

    void Awake()
    {
        myTransform = transform;
    }

    public void Init(System.Action<Enemy> OnDeadAction, Transform playerTransform)
    {
        this.playerTransform = playerTransform;
        OnDead = OnDeadAction;
        if (!initializedComponents)
        {
            initializedComponents = true;
            if (enemyViewer != null) enemyViewer.Init(ref OnPlayAnimation);
            if (moveController != null) moveController.Init(stateData, ref OnChaseTarget, ref OnMoveToPosition, ref OnStopMove, ref OnLookAt, ref OnStopLookAt, ref OnPlayAnimation);
            if (attackController != null) attackController.Init(stateData, ref OnStartAttack, ref OnStopAttack, ref OnPlayAnimation);
            if (healthController != null)
            {
                healthController.Init(stateData.enemyData);
                healthController.OnDamage += OnDamaged;
                healthController.OnDeath += Death;
            }
            stateData.Init();
            CacheBehaviours();
        }
        else
        {
            if (healthController != null) healthController.ResetHealth();
        }
    }

    public void Activate()
    {
        deployCoroutine = StartCoroutine(WaitDeploy());
    }

    IEnumerator WaitDeploy()
    {
        yield return CoroutineTools.GetYield(stateData.enemyData.DeployTime);
        SetCurrentBehaviour(EnemyState.Idle);
        GameManager.UpdateManager.SubscribeForUpdate(this);
    }

    public void Deactivate()
    {
        if (moveController != null) moveController.Deactivate();
        if (attackController != null) attackController.Deactivate();
        if (healthController != null) healthController.Deactivate();
        CoroutineTools.StopAndNullCoroutine(ref deployCoroutine, this);
        currentBehaviour.Deactivate();
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
    }

    void CacheBehaviours()
    {
        var presentBehaviours = GetComponents<IEnemyBehaviour>();
        int behavioursCount = presentBehaviours.Length;
        for (int i = 0; i < behavioursCount; i++)
        {
            Hashtable parameters = null;
            switch (presentBehaviours[i].BehaviourType)
            {
                case EnemyState.Idle:break;
                case EnemyState.Attacking:
                    parameters = new Hashtable()
                    {
                        { Constants.STATE_DATA , stateData },
                        { Constants.PLAYER_TRANSFORM , playerTransform }
                    };
                    break;
            }
            presentBehaviours[i].Init(parameters, OnChaseTarget, OnMoveToPosition, OnStopMove, OnStartAttack, OnStopAttack, OnLookAt, OnStopLookAt, OnPlayAnimation);
            behaviours.Add((int)presentBehaviours[i].BehaviourType, presentBehaviours[i]);
        }
    }

    public void SetCurrentBehaviour(EnemyState behaviourType)
    {
        int behTypeInt = (int)behaviourType;
        bool behaviourAvialable = behaviours.ContainsKey(behTypeInt);
        if (behaviourAvialable)
        {
            if (currentBehaviour != null) currentBehaviour.Deactivate();
            currentBehaviour = behaviours[behTypeInt];
            currentBehaviour.Activate();
            stateData.ChangeState(behaviourType);
        }
    }

    public void OnUpdate()
    {
        if (stateData.currentState != EnemyState.Attacking && (playerTransform.position - myTransform.position).sqrMagnitude < stateData.enemyData.AgroDistance)
        {
            SetCurrentBehaviour(EnemyState.Attacking);
            EnemyWarning();
        }
    }

    void EnemyWarning()
    {
        if (currentBehaviour.BehaviourType == EnemyState.Attacking)
        {
            BattleManager.EnemiesController.EnemyWarning(myTransform.position, stateData.enemyData.WarningRadius);
        }
    }

    void PlayAttackAnimation()
    {
        OnPlayAnimation?.Invoke(AnimationEnum.Attack);
    }

    void OnDamaged(float damage)
    {
        switch (stateData.currentState)
        {
            case EnemyState.Attacking:break;
            default: 
                SetCurrentBehaviour(EnemyState.Attacking);
                EnemyWarning();
                break;
        }
    }

    void Death()
    {
        Deactivate();
        BattleManager.PoolManager.PushObject(gameObject);
        OnDead?.Invoke(this);
    }
}
