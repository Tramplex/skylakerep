﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIdleBehaviour : MonoBehaviour, IEnemyBehaviour, IUpdatable
{
    public bool SubscribedForUpdates { get; set; }
    [SerializeField] Vector2 randomTravelDistanceMinMax;
    [SerializeField] Vector2 randomTravelCooldownMinMax;
    public EnemyState BehaviourType { get { return EnemyState.Idle; } }
    Transform myTransform;
    System.Action<Transform> ChaseOrder;
    System.Action<Vector3> MoveOrder;
    System.Action StopMoveOrder;
    System.Action<Transform, EnemyAttackType> AttackOrder;
    System.Action StopAttackOrder;
    Vector2 boundPosition;
    float randomTravelCooldown;
    float travelDistance;
    Vector3 randomTravelPosition;

    /// <summary>
    /// Will bind to position where initialized
    /// </summary>
    public void Init(Hashtable parameters
        , System.Action<Transform> ChaseOrderAction
        , System.Action<Vector3> MoveOrderAction
        , System.Action StopMoveOrderAction
        , System.Action<Transform, EnemyAttackType> AttackOrderAction
        , System.Action StopAttackOrderAction
        , System.Action<Transform> LookAtOrderAction
        , System.Action StopLookAtOrderAction
        , System.Action<AnimationEnum> PlayAnimationOrderAction)
    {
        myTransform = transform;
        MoveOrder = MoveOrderAction;
        StopMoveOrder = StopMoveOrderAction;
        AttackOrder = AttackOrderAction;
        StopAttackOrder = StopAttackOrderAction;
    }

    public void Activate()
    {
        boundPosition = myTransform.position;
        waitTimer = 0f;
        GameManager.UpdateManager.SubscribeForUpdate(this);
    }

    public void Deactivate()
    {
        StopMoveOrder?.Invoke();
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
    }

    float waitTimer;
    public void OnUpdate()
    {
        waitTimer += Time.deltaTime;
        if(waitTimer > randomTravelCooldown)
        {
            waitTimer -= randomTravelCooldown;
            randomTravelCooldown = Random.Range(randomTravelCooldownMinMax.x, randomTravelCooldownMinMax.y);
            travelDistance = Random.Range(randomTravelDistanceMinMax.x, randomTravelDistanceMinMax.y);
            randomTravelPosition = boundPosition + Random.insideUnitCircle.normalized * travelDistance;
            MoveOrder?.Invoke(randomTravelPosition);
        }
    }
}
