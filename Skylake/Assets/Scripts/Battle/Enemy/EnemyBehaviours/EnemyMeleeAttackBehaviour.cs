﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMeleeAttackBehaviour : MonoBehaviour, IEnemyBehaviour, IUpdatable
{
    public bool SubscribedForUpdates { get; set; }
    public EnemyState BehaviourType { get { return EnemyState.Attacking; } }
    EnemyStateData stateData;
    Transform playerTransform;
    Transform myTransform;
    System.Action<Transform> ChaseOrder;
    System.Action<Vector3> MoveOrder;
    System.Action StopMoveOrder;
    System.Action<Transform, EnemyAttackType> AttackOrder;
    System.Action StopAttackOrder;
    System.Action<Transform> LookAtOrder;
    System.Action StopLookAtOrder;
    System.Action<AnimationEnum> PlayAnimationOrder;

    public void Init(Hashtable parameters
        , System.Action<Transform> ChaseOrderAction
        , System.Action<Vector3> MoveOrderAction
        , System.Action StopMoveOrderAction
        , System.Action<Transform, EnemyAttackType> AttackOrderAction
        , System.Action StopAttackOrderAction
        , System.Action<Transform> LookAtOrderAction
        , System.Action StopLookAtOrderAction
        , System.Action<AnimationEnum> PlayAnimationOrderAction)
    {
        myTransform = transform;
        if (parameters.ContainsKey(Constants.STATE_DATA)) stateData = parameters[Constants.STATE_DATA] as EnemyStateData;
        if (parameters.ContainsKey(Constants.PLAYER_TRANSFORM)) playerTransform = parameters[Constants.PLAYER_TRANSFORM] as Transform;
        ChaseOrder = ChaseOrderAction;
        MoveOrder = MoveOrderAction;
        StopMoveOrder = StopMoveOrderAction;
        AttackOrder = AttackOrderAction;
        StopAttackOrder = StopAttackOrderAction;
        LookAtOrder = LookAtOrderAction;
        StopLookAtOrder = StopLookAtOrderAction;
        PlayAnimationOrder = PlayAnimationOrderAction;
    }

    public void Activate()
    {
        waitTimer = 0f;
        GameManager.UpdateManager.SubscribeForUpdate(this);
    }

    public void Deactivate()
    {
        StopAttackOrder?.Invoke();
        StopMoveOrder?.Invoke();
        StopLookAtOrder?.Invoke();
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
    }

    bool CloseEnoughToPlayer()
    {
        return (myTransform.position - playerTransform.position).sqrMagnitude < stateData.enemyData.ReachDistance;
    }

    float waitTimer;
    public void OnUpdate()
    {
        waitTimer += Time.deltaTime;
        if (stateData.isMoving)
        {

        }
        else if (stateData.isAttacking)
        {

        }
        else
        {
            if (CloseEnoughToPlayer())
            {
                AttackOrder?.Invoke(playerTransform, EnemyAttackType.SimpleMelee);
                LookAtOrder?.Invoke(playerTransform);
            }
            else
            {
                StopLookAtOrder?.Invoke();
                ChaseOrder?.Invoke(playerTransform);
            }
        }
    }
}
