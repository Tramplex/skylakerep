﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMeleeAttack : MonoBehaviour, IEnemyAttack, IUpdatable
{
    public bool SubscribedForUpdates { get; set; }
    public EnemyAttackType attackType { get { return EnemyAttackType.SimpleMelee; } }
    public event System.Action OnAttackFinished;
    EnemyStateData stateData;
    RaycastHit2D hit;
    Transform myTransform;
    bool hitArea = false;

    void Awake()
    {
        myTransform = transform;
    }

    public void Init(EnemyStateData stateData)
    {
        this.stateData = stateData;
    }

    public void StartAttack(Transform attackTarget)
    {
        waitTimer = 0;
        hitArea = false;
        GameManager.UpdateManager.SubscribeForUpdate(this);
    }

    float waitTimer;
    public void OnUpdate()
    {
        waitTimer += Time.deltaTime;
        if(!hitArea && waitTimer > stateData.enemyData.AttackAnimationDelay)
        {
            waitTimer = 0f;
            HitArea();
        }
        else if (waitTimer > stateData.enemyData.AttackCooldownTime)
        {
            OnAttackFinished?.Invoke();
            CancelAttack();
        }
    }

    void HitArea()
    {
        Vector2 hitDirection = myTransform.up;
        hit = Physics2D.Raycast(myTransform.position, hitDirection, stateData.enemyData.AttackDistance, Constants.PLAYER_LAYER_MASK);
        if (hit)
        {
            var healthController = hit.collider.GetComponent<HealthController>();
            if(healthController != null && healthController.faction == FactionType.Player)
            {
                healthController.ChangeHealthValue(-Random.Range(stateData.enemyData.MinDamage, stateData.enemyData.MaxDamage), hitDirection);
            }
        }
        hitArea = true;
    }

    public void CancelAttack()
    {
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
    }
}
