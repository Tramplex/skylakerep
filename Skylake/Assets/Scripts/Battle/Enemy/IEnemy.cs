﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemy
{
    EnemyState CurrentState { get; }
    void Init(System.Action<Enemy> OnDeadAction);
    void MoveToPosition(Vector3 position);
    void StartAttackTarget(Transform targetTransform);
    void ChaseTarget(Transform targetTransform);
    void Stop();
}
