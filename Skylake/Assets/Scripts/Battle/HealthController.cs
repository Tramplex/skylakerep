﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    public bool invulnerable;
    public FactionType faction;
    [SerializeField] Transform healthBarPoint;
    [SerializeField] Transform hitPoint;
    [SerializeField] HealthBarType healthBarType;
    [SerializeField] IDEnum takeHitEffect;
    [SerializeField] IDEnum bloodEffect;
    HealthBar healthBar;
    public float MaxHp { get { return maxHp; } }
    float maxHp;
    float currentHp;
    Transform myTransform;
    public float CurrentHp { get { return currentHp; } }
    public event System.Action OnDeath;
    /// <summary>
    /// returns new value of health
    /// </summary>
    public event System.Action<float> OnDamage;
    public event System.Action<float> OnHealthChange;

    private void Awake()
    {
        myTransform = transform;
    }

    public void Init(UnitData data)
    {
        faction = data.Faction;
        maxHp = currentHp = data.MaxHp;
    }

    public void ResetHealth()
    {
        currentHp = maxHp;
    }

    public void Deactivate()
    {
        currentHp = 0;
    }

    public void ChangeHealthValue(float healthChange, Vector3 hitDirection)
    {
        if (invulnerable)
        {
            return;
        }
        if(healthBar == null && healthBarType != HealthBarType.NotDefined)
        {
            healthBar = BattleManager.HealthBarCanvas.GetHealthBar(healthBarType);
            healthBar.Init(this, healthBarPoint);
        }
        if(healthChange < 0)
        {
            if (takeHitEffect > 0)
            {
                BattleManager.PoolManager.PullObject(takeHitEffect, hitPoint.position, UtilityTools.GetRandomZRotationFromDirection(ref hitDirection, 10f));
            }
            if (bloodEffect > 0)
            {
                BattleManager.PoolManager.PullObject(bloodEffect, myTransform.position, UtilityTools.GetRandomZRotationFromDirection(ref hitDirection, 10f));
            }
        }
        currentHp = Mathf.Clamp(currentHp + healthChange, 0f, maxHp);
        OnHealthChange?.Invoke(currentHp);
        if(healthChange < 0)
        {
            OnDamage?.Invoke(healthChange);
        }
        if (currentHp <= 0f)
        {
            healthBar = null;
            OnDeath?.Invoke();
        }
    }
}
