﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PooledObject))]
public class Bullet : MonoBehaviour, IUpdatable, IFixedUpdatable
{
    public bool SubscribedForUpdates { get; set; }
    public bool SubscribedForFixedUpdates { get; set; }
    [SerializeField] BulletData data;
    [SerializeField] PooledObject pooledObject;
    IDEnum hitPrefabID;
    float speed;
    float lifetime;
    float minDamage;
    float maxDamage;
    float currentTime;
    float deleteTime;
    int damage;
    int hitMask;
    int hitsCount = 0;
    bool orientEffectToHitNormal;
    Vector2 rayOrigin;
    Vector2 rayDirection;
    RaycastHit2D[] hits = new RaycastHit2D[3];
    HealthController hitTarget;
    Transform myTransform;
    bool bulletHitObject;
    PopUpTextsController popUpTextController;

    private void Awake()
    {
        popUpTextController = BattleManager.PopUpTextController;
        myTransform = transform;
        speed = data.Speed * Time.fixedDeltaTime;
        lifetime = data.Lifetime;
        minDamage = data.MinDamage;
        maxDamage = data.MaxDamage;
        hitPrefabID = data.HitPrefabId;
        orientEffectToHitNormal = data.OrientEffectToHitNormal;
        hitMask = (1 << Constants.ENEMY_LAYER_MASK) | (1 << Constants.OBSTACLE_LAYER_MASK) | data.HitMask;
    }

    public void Init()
    {
        bulletHitObject = false;
        currentTime = Time.time;
        deleteTime = currentTime + lifetime;
        rayOrigin = myTransform.position;
        rayDirection = myTransform.up;
        GameManager.UpdateManager.SubscribeForUpdate(this);
        GameManager.UpdateManager.SubscribeForUpdateFixed(this);
    }

    public void OnUpdate()
    {
        myTransform.position = Vector3.Lerp(myTransform.position, rayOrigin, 0.5f);
        if (bulletHitObject)
        {
            Hit();
        }
        else
        {
            if (currentTime < deleteTime)
            {
                currentTime += Time.deltaTime;
            }
            else
            {
                GameManager.UpdateManager.UnsubscribeFromUpdate(this);
                GameManager.UpdateManager.UnsubscribeFromUpdateFixed(this);
                BattleManager.PoolManager.PushObject(gameObject);
            }
        }
    }

    public void OnFixedUpdate()
    {
        hitsCount = Physics2D.RaycastNonAlloc(rayOrigin, rayDirection, hits, speed, hitMask);
        if (hitsCount > 0)
        {
            bulletHitObject = true;
        }
        else
        {
            rayOrigin = rayOrigin + rayDirection * speed;
        }
    }

    void Hit()
    {
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
        GameManager.UpdateManager.UnsubscribeFromUpdateFixed(this);
        myTransform.position = hits[0].point;
        Quaternion hitEffectRotation = orientEffectToHitNormal ? Quaternion.LookRotation(hits[0].normal) : Quaternion.identity;
        BattleManager.PoolManager.PullObject(hitPrefabID, hits[0].point, hitEffectRotation);
        hitTarget = hits[0].collider.gameObject.GetComponent<HealthController>();
        if (hitTarget != null)
        {
            damage = Mathf.RoundToInt(Random.Range(minDamage, maxDamage));
            popUpTextController.PopNumberInWorldPosition(damage, hits[0].transform.position);
            hitTarget.ChangeHealthValue(-damage, myTransform.up);
        }
        pooledObject.PushToPool();
    }
}
