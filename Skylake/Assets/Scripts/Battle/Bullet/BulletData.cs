﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "TheSurface/New BulletData")]
public class BulletData : ScriptableObject
{
    [SerializeField] IDEnum hitPrefabId;
    public IDEnum HitPrefabId { get { return hitPrefabId; } }

    [SerializeField] float lifetime;
    public float Lifetime { get { return lifetime; } }

    [SerializeField] float removeDelay;
    public float RemoveDelay { get { return removeDelay; } }

    [SerializeField] float minDamage;
    public float MinDamage { get { return minDamage; } }

    [SerializeField] float maxDamage;
    public float MaxDamage { get { return maxDamage; } }

    [SerializeField] float speed;
    public float Speed { get { return speed; } }

    [SerializeField] bool orientEffectToHitNormal;
    public bool OrientEffectToHitNormal { get { return orientEffectToHitNormal; } }

    [SerializeField] LayerMask hitMask;
    public LayerMask HitMask { get { return hitMask; } }
}
