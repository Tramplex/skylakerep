﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DataHolders/New UnitData")]
public class UnitData : ScriptableObject
{
    public FactionType Faction;
    public float DeployTime;
    public float MaxHp;
    public float MinDamage;
    public float MaxDamage;
    public float AttackCooldownTime;
    public float AttackAnimationDelay;
    public float AttackDistance;
    public float ReachDistance;
    public float IdleMoveSpeed;
    public float AttackMoveSpeed;
    public float MoveWiggle;
    public float RotationSpeed;
    public float RotationAcceleration;
    public bool AgroOnDamage = true;
    public float AgroDistance;
    public float WarningRadius;
    public EnemyState InitialState = EnemyState.Idle;
}
