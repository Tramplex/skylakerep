﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour, IUpdatable
{
    public static float movedDistance { get; private set; }
    public static float currentMaxSpeed { get; private set; }
    public bool SubscribedForUpdates { get; set; }
    public Transform moveTransform { get { return rigidTransform; } }
    [SerializeField] float positionLerpSpeed;
    [SerializeField] Transform modelHolder;
    [SerializeField] Rigidbody2D rigid;
    [SerializeField] Transform rigidTransform;
    public static float maxSpeed { get; private set; }
    public static float maxRunSpeed { get; private set; }
    InputManager InputManager;
    Vector2 lastPosition;
    Vector2 currentPosition;
    Vector2 currentMovement;
    System.Action<Vector2> positionChangeAction;

    void Awake()
    {
        currentPosition = lastPosition = modelHolder.position;
    }

    public void Init(UnitData data, ref System.Action<Vector2> OnPositionChange)
    {
        CameraFollow.target = modelHolder;
        InputManager = GameManager.InputManager;
        positionChangeAction = OnPositionChange;
        currentMaxSpeed = maxSpeed = data.IdleMoveSpeed;
        maxRunSpeed = data.AttackMoveSpeed;
        InputManager.OnPlayerMoveEnd += MoveEnd;
        InputManager.OnPlayerRunStart += RunStart;
        InputManager.OnPlayerRunEnd += RunEnd;
        GameManager.UpdateManager.SubscribeForUpdate(this);
    }

    public void Deactivate()
    {
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
        InputManager.OnPlayerMoveEnd -= MoveEnd;
        InputManager.OnPlayerRunStart -= RunStart;
        InputManager.OnPlayerRunEnd -= RunEnd;
    }

    void RunStart()
    {
        currentMaxSpeed = maxRunSpeed;
    }

    void RunEnd()
    {
        currentMaxSpeed = maxSpeed;
    }

	void MoveEnd()
	{
        positionChangeAction?.Invoke(Vector3.zero);
    }

	public void OnUpdate()
	{
        modelHolder.position = Vector3.Slerp(modelHolder.position, rigidTransform.position, positionLerpSpeed * Time.deltaTime);
        //currentMovement = new Vector2(InputManager.moveInput.x, InputManager.moveInput.y) * maxSpeed * Time.deltaTime;
        //myTransform.Translate(currentMovement);
        currentPosition = rigidTransform.position;
        var currentMoveInWorld = lastPosition - currentPosition;
        movedDistance = currentMoveInWorld.magnitude;
        positionChangeAction?.Invoke(currentMoveInWorld);
        float traveledDistance = Vector3.Distance(currentPosition, lastPosition) / Time.deltaTime;
        PlayerEventManager.Event(new PlayerMovedEventData(traveledDistance));
        lastPosition = currentPosition;
    }

    private void FixedUpdate()
    {
        currentMovement = new Vector2(InputManager.moveInput.x, InputManager.moveInput.y) * currentMaxSpeed;
        //rigid.MovePosition((Vector2)rigidTransform.position + currentMovement);
        rigid.velocity = currentMovement;
    }
}
