﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAim : MonoBehaviour, IUpdatable
{
    public bool SubscribedForUpdates { get; set; }
    [SerializeField] Transform modelTransform;
	[SerializeField] float playerAimedThreshold;
	[SerializeField] float rotationLerpSpeed;
	[SerializeField] float waitToAffectRotationTimeout;
    InputManager inputManager;
	bool isAiming;
	bool isMoving = false;
	bool aimed = false;
	bool walkAffectRotation = true;
	float rotationSpeed;
	float wantedAngle;
	public float currentAngle;
	Coroutine waitWalkToAffectRotationCoroutine;
    System.Action<float> yRotationChangeAction;

    public void Init(UnitData data, ref System.Action<float> OnYRotationChange)
    {
        rotationSpeed = data.RotationSpeed;
        yRotationChangeAction = OnYRotationChange;
        inputManager = GameManager.InputManager;
        inputManager.OnPlayerAimStart += AimStart;
        inputManager.OnPlayerAimEnd += AimEnd;
        inputManager.OnPlayerMoveStart += MoveStart;
        inputManager.OnPlayerMoveEnd += MoveEnd;
        GameManager.UpdateManager.SubscribeForUpdate(this);
        currentAngle = 180f;
    }

    public void Deactivate()
    {
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
        inputManager.OnPlayerAimStart -= AimStart;
        inputManager.OnPlayerAimEnd -= AimEnd;
        inputManager.OnPlayerMoveStart -= MoveStart;
        inputManager.OnPlayerMoveEnd -= MoveEnd;
    }

	void MoveStart()
	{
		isMoving = true;
	}

	void MoveEnd()
	{
		isMoving = false;
	}

	void AimStart()
	{
		isAiming = true;
		walkAffectRotation = false;
		CoroutineTools.StopAndNullCoroutine(ref waitWalkToAffectRotationCoroutine, this);
	}

	void AimEnd()
	{
        aimed = false;
        isAiming = false;
		waitWalkToAffectRotationCoroutine = StartCoroutine(WaitWalkToAffectRotation());
	}

	public void OnUpdate()
	{
		if (isAiming)
		{
			wantedAngle = Mathf.Atan2(inputManager.aimInput.x, inputManager.aimInput.y) * Mathf.Rad2Deg + 180f;
            currentAngle = Mathf.MoveTowardsAngle(currentAngle, wantedAngle, rotationSpeed * Time.deltaTime);
            if (Mathf.Abs(wantedAngle - currentAngle) < playerAimedThreshold)
            {
                if (!aimed)
                {
                    aimed = true;
                    PlayerEventManager.Event(TypeOfEvent.PlayerAimed);
                }
            }
            else
            {
                if (aimed)
                {
                    aimed = false;
                    PlayerEventManager.Event(TypeOfEvent.PlayerNotAimed);
                }
            }
            if (currentAngle < 0)
			{
                currentAngle += 360f;
			}
			else if (currentAngle > 360f)
			{
                currentAngle -= 360f;
			}
			if (currentAngle - 180f != 0f)
			{
                modelTransform.rotation = Quaternion.Euler(new Vector3(0f, 0f, -currentAngle + 180f));
                yRotationChangeAction?.Invoke(currentAngle);
            }
		}
		else if (isMoving && walkAffectRotation)
		{
			wantedAngle = Mathf.Atan2(inputManager.moveInput.x, inputManager.moveInput.y) * Mathf.Rad2Deg + 180f;
            currentAngle = Mathf.MoveTowardsAngle(currentAngle, wantedAngle, rotationSpeed * Time.deltaTime);
            if (currentAngle < 0)
            {
                currentAngle += 360f;
            }
            else if (currentAngle > 360f)
            {
                currentAngle -= 360f;
            }

            if (currentAngle - 180f != 0f)
            {
                modelTransform.rotation = Quaternion.Euler(new Vector3(0f, 0f, -currentAngle + 180f));
                yRotationChangeAction?.Invoke(currentAngle);
            }
        }
	}

	IEnumerator WaitWalkToAffectRotation()
	{
		yield return CoroutineTools.GetYield(waitToAffectRotationTimeout);
		walkAffectRotation = true;
		waitWalkToAffectRotationCoroutine = null;
	}
}
