﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] UnitData playerData;
    [SerializeField] PlayerAim aimController;
    [SerializeField] PlayerAnimation animationController;
    [SerializeField] PlayerMove moveController;
    [SerializeField] PlayerWeapon weaponController;
    [SerializeField] HealthController playerHealthController;
    public Transform moveTransform { get { return moveController.moveTransform; } }
    public HealthController PlayerHealth { get { return playerHealthController; } }
    public UnitData PlayerData { get { return playerData; } }

    public static System.Action<Vector2> OnPositionChange;
    public static System.Action<float> OnLookDirectionChange;
    public static System.Action<bool> OnPrepareShoot;
    public static System.Action OnShoot;

    BattleManager battleManager;

    public void Init(BattleManager battleManager)
    {
        this.battleManager = battleManager;
        //if (viewerController != null)
        //{
        //    viewerController.Init(ref OnPositionChange, ref OnLookDirectionChange, ref OnPrepareShoot, ref OnShoot);
        //}
        if (weaponController != null) weaponController.Init(ref OnPrepareShoot, ref OnShoot);
        if (moveController != null) moveController.Init(playerData, ref OnPositionChange);
        if (aimController != null) aimController.Init(playerData, ref OnLookDirectionChange);
        if (playerHealthController != null)
        {
            playerHealthController.Init(playerData);
            playerHealthController.OnDeath += PlayerDead;
        }
        if (animationController != null) animationController.Init(playerData);
    }

    void PlayerDead()
    {
        battleManager.BattleFinished();
    }

    public void Deactivate()
    {
        if (moveController != null) moveController.Deactivate();
        if (aimController != null) aimController.Deactivate();
        if (weaponController != null) weaponController.Deactivate();
        if (playerHealthController != null) playerHealthController.Deactivate();
        if (animationController != null) animationController.Deactivate();
        OnPositionChange = null;
        OnLookDirectionChange = null;
    }
}
