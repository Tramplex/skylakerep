﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField] Animator anim;
    float maxWalkSpeed;
    float maxRunSpeed;
    int shootHash;
    int reloadHash;
    int reloadSpeedHash;
    int moveHash;

    public void Init(UnitData data)
    {
        shootHash = Constants.GetAnimationHash(AnimationEnum.Attack);
        reloadHash = Constants.GetAnimationHash(AnimationEnum.Reload);
        reloadSpeedHash = Constants.GetAnimationHash(AnimationEnum.ReloadSpeed);
        moveHash = Constants.GetAnimationHash(AnimationEnum.Move);
        maxWalkSpeed = data.IdleMoveSpeed;
        maxRunSpeed = data.AttackMoveSpeed;
        PlayerEventManager.OnWeaponEquipped += PlayerEquipWeapon;
        PlayerEventManager.OnPlayerMoved += PlayerMoved;
        PlayerEventManager.OnShootWeapon += PlayerShot;
        PlayerEventManager.OnPlayerWeaponStartReload += PlayerStartedReload;
    }

    public void Deactivate()
    {
        PlayerEventManager.OnWeaponEquipped -= PlayerEquipWeapon;
        PlayerEventManager.OnPlayerMoved -= PlayerMoved;
        PlayerEventManager.OnShootWeapon -= PlayerShot;
        PlayerEventManager.OnPlayerWeaponStartReload -= PlayerStartedReload;
    }

    void PlayerEquipWeapon(WeaponEquippedEventData eventData)
    {
        anim.SetFloat(reloadSpeedHash, 1 / eventData.equippedWeapon.WeaponData.ReloadTime);
    }

    void PlayerShot()
    {
        ResetTriggers();
        anim.SetTrigger(shootHash);
    }

    void PlayerStartedReload()
    {
        ResetTriggers();
        anim.SetTrigger(reloadHash);
    }

    void PlayerMoved(PlayerMovedEventData eventData)
    {
        float relativeMove = 0f;
        if(eventData.move > maxWalkSpeed)
        {
            float relativeRun = Mathf.InverseLerp(maxWalkSpeed, maxRunSpeed, eventData.move);
            relativeMove = 0.5f + relativeRun * 0.5f;
        }
        else
        {
            float relativeWalk = Mathf.InverseLerp(0, maxWalkSpeed, eventData.move);
            relativeMove = relativeWalk * 0.5f;
        }
        anim.SetFloat(moveHash, relativeMove);
    }

    void ResetTriggers()
    {
        anim.ResetTrigger(shootHash);
        anim.ResetTrigger(reloadHash);
    }
}
