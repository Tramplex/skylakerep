﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationEvents : MonoBehaviour
{
    public void ShowMagazine()
    {
        PlayerEventManager.Event(TypeOfEvent.ShowMagazine);
    }

    public void HideMagazine()
    {
        PlayerEventManager.Event(TypeOfEvent.HideMagazine);
    }

    public void DropMagazine()
    {
        PlayerEventManager.Event(TypeOfEvent.DropMagazine);
    }
}
