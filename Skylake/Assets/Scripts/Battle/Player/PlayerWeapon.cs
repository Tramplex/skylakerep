﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    [SerializeField] Transform modelHolder;
	[SerializeField] Transform weaponsHolder;
	[SerializeField] Transform magazineHolder;
    [SerializeField] float aimAssistMinimalDistance;
    [SerializeField] float aimAssistDistance;
    [SerializeField] float aimAssistWidth;
    [SerializeField] LayerMask aimAssistLayer;
    //[SerializeField] AmmoCounter ammoCounter;
    Weapon currentWeapon;
    Coroutine waitAutoReloadCoroutine;
    Coroutine aimAssistCoroutine;
    bool autoReloadEnabled;
    RaycastHit2D[] hits = new RaycastHit2D[3];
    Ray ray;
    int hitsCount;
    Vector3 targetPosition;
    Vector3 forwardVector;
    System.Action<bool> prepareShootAction;
    System.Action shootAction;
    bool aimAssistActive = false;

    private void Update()
    {
        if (aimAssistActive) AimAssist();
    }

    public void Init(ref System.Action<bool> OnPrepareShoot, ref System.Action OnShoot)
	{
        prepareShootAction = OnPrepareShoot;
        shootAction = OnShoot;
        //ammoCounter.Init();
        autoReloadEnabled = GameManager.SettingsData.AutoReloadInBattle;
        EventManager.OnBattleStart += BattleStart;
        EventManager.OnBattleEnd += BattleEnd;
    }

    void BattleStart()
    {
        EventManager.OnBattleStart -= BattleStart;
        ChangeWeapon(IDEnum.MachinegunID);
        Activate();
    }

    void BattleEnd()
    {
        EventManager.OnBattleEnd -= BattleEnd;
    }

    void Activate()
    {
        PlayerEventManager.OnPlayerManualReload += ReloadCurrentWeapon;
        PlayerEventManager.OnPlayerAimed += StartShooting;
        PlayerEventManager.OnPlayerStartAim += StartAimAssist;
        PlayerEventManager.OnPlayerEndAim += EndShooting;
    }

    public void Deactivate()
    {
        PlayerEventManager.OnPlayerManualReload -= ReloadCurrentWeapon;
        PlayerEventManager.OnPlayerAimed -= StartShooting;
        PlayerEventManager.OnPlayerStartAim -= StartAimAssist;
        PlayerEventManager.OnPlayerEndAim -= EndShooting;
        currentWeapon.Deactivate();
    }

	public void ChangeWeapon(IDEnum id)
	{
		if(currentWeapon != null)
		{
            currentWeapon.Deactivate();
            BattleManager.PoolManager.PushObject(currentWeapon.gameObject);
		}
		GameObject weaponObject = BattleManager.PoolManager.PullObject(id, weaponsHolder.position, weaponsHolder.rotation);
        weaponObject.transform.SetParent(weaponsHolder);
        currentWeapon = weaponObject.GetComponent<Weapon>();
		currentWeapon.Init(magazineHolder);
        currentWeapon.OnShotFired += WeaponShot;
        aimAssistDistance = currentWeapon.WeaponData.AimAssistDistance;
        aimAssistWidth = currentWeapon.WeaponData.AimAssistWidth;
        PlayerEventManager.Event(new WeaponEquippedEventData(currentWeapon));
	}

    void StartShooting()
    {
        currentWeapon.StartShooting();
        prepareShootAction?.Invoke(true);
    }

    void StartAimAssist()
    {
        aimAssistActive = true;
        if (autoReloadEnabled)
        {
            CoroutineTools.StopAndNullCoroutine(ref waitAutoReloadCoroutine, this);
        }
    }

    void EndShooting()
    {
        currentWeapon.StopShooting();
        prepareShootAction?.Invoke(false);
        if (autoReloadEnabled)
        {
            waitAutoReloadCoroutine = StartCoroutine(WaitAutoReloadWeapon());
        }
        aimAssistActive = false;
    }
    
    void WeaponShot()
    {
        shootAction?.Invoke();
    }

    IEnumerator WaitAutoReloadWeapon()
    {
        yield return CoroutineTools.GetYield(Constants.AUTO_RELOAD_TIME);
        currentWeapon.StartWeaponReload();
    }

    void ReloadCurrentWeapon()
    {
        currentWeapon.StartWeaponReload();
    }

    void AimAssist()
    {
        forwardVector = modelHolder.up;
        var origin = modelHolder.position + forwardVector * aimAssistMinimalDistance;
        var direction = forwardVector;
        var coneHit = Physics2DExtentions.ConeCast2D(origin, direction, aimAssistDistance, 0.1f, aimAssistWidth);
        if (coneHit.hitSomething)
        {
            targetPosition = coneHit.hit.collider.transform.position;
            weaponsHolder.up = targetPosition - modelHolder.position;
        }
        else
        {
            weaponsHolder.localRotation = Quaternion.identity;
        }
    }

    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.blue;
        //Gizmos.DrawWireSphere(modelHolder.position + modelHolder.up * aimAssistMinimalDistance, aimAssistWidth);
        //Gizmos.DrawWireSphere(modelHolder.position + modelHolder.up * aimAssistDistance, aimAssistWidth);
    }
}

