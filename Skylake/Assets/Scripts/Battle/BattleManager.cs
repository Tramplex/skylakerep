﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TS.UI;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    [SerializeField] float gameStartDelay;
    [SerializeField] PopUpTextsController popUpTextController;
    public static PopUpTextsController PopUpTextController { get; private set; }
    [SerializeField] HealthBarCanvas healthBarCanvas;
    public static HealthBarCanvas HealthBarCanvas { get; private set; }
    [SerializeField] PoolManager poolManager;
    public static PoolManager PoolManager { get; private set; }
    [SerializeField] EnemiesController enemiesController;
    public static EnemiesController EnemiesController { get; private set; }
    [SerializeField] DistanceChecker distanceChecker;
    public static DistanceChecker DistanceChecker { get; private set; }
    public static Player Player { get; private set; }
    List<EnemiesZone> enemiesZones = new List<EnemiesZone>();
    int enemiesZonesCount;

    public void Awake()
    {
        PopUpTextController = popUpTextController;
        HealthBarCanvas = healthBarCanvas;
        PoolManager = poolManager;
        PoolManager.Init();
        DistanceChecker = distanceChecker;
        EnemiesController = enemiesController;
        EnemiesController.Init();
        EventManager.OnGameSceneLoaded += BattleSceneLoaded;
        EventManager.OnHubSceneLoaded += BattleSceneLoaded;
    }

    void BattleSceneLoaded()
    {
        Camera mainCamera = Camera.main;
        poolManager.ResetPool();
        popUpTextController.Init(mainCamera);
        //HealthBarCanvas.Init(mainCamera);
        GameObject playerObject = poolManager.PullObject(IDEnum.PlayerID, Vector3.zero, Quaternion.identity);
        var spawnedPlayer = playerObject.GetComponent<Player>();
        spawnedPlayer.Init(this);
        Player = spawnedPlayer;
        distanceChecker.Init(Player.moveTransform);
        distanceChecker.ResetCheck();
        enemiesZones = FindObjectsOfType<EnemiesZone>().ToList();
        enemiesZonesCount = enemiesZones.Count;
        for (int i = 0; i < enemiesZonesCount; i++)
        {
            enemiesZones[i].Init();
        }
        StartCoroutine(WaitStartBattle());
    }

    IEnumerator WaitStartBattle()
    {
        yield return null;
        var ingameWindow = GameManager.WindowManager.OpenWindow(WindowType.InGame) as InGameWindow;
        ingameWindow.Init();
        yield return CoroutineTools.GetYield(gameStartDelay);
        BeginBattle();
    }

    public void BeginBattle()
	{
        EventManager.Event(TypeOfEvent.BattleStart);
	}

    public void BattleFinished()
    {
        Player.Deactivate();
        for (int i = 0; i < enemiesZonesCount; i++)
        {
            enemiesZones[i].Deactivate();
        }
        EventManager.Event(TypeOfEvent.BattleEnd);
    }

    public void OnBattleSceneUnload()
    {

    }
}
