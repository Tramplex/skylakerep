﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TheSurface/New PrefabsDatabase")]
public class PrefabsDatabase : ScriptableObject
{
    [SerializeField] public List<DatabaseItem> items;

    public GameObject GetPrefab(int id)
    {
        if(items.Count > id)
        {
            return items[id].prefabObject;
        }
        else
        {
            return null;
        }
    }

    [System.Serializable]
    public class DatabaseItem
    {
        public string name;
        public GameObject prefabObject;
    }
}
