﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesZone : MonoBehaviour, IDistanceChecker
{
    [SerializeField] EnemyType enemyId;
    [SerializeField] float randomSpawnRadius;
    [SerializeField] int maxEnemiesCount;
    [SerializeField] int enemiesCount;
    int spawnedEnemies = 0;
    Transform playerTransform;
    Transform myTransform;
    List<Enemy> activeEnemies = new List<Enemy>();
    public Transform CheckTransform { get { return myTransform; } }
    public bool IsChecked { get; set; }
    public DistanceType CurrentDistanceType { get; set; }

    private void Awake()
    {
        myTransform = transform;
    }

    public void Init()
    {
        spawnedEnemies = 0;
        playerTransform = BattleManager.Player.moveTransform;
        DistanceChecker.AddChecker(this);
    }

    public void OnDistanceChange(DistanceType newDistance)
    {
        if (newDistance <= DistanceType.Far)
        {
            int enemiesToSpawn = Mathf.Min(maxEnemiesCount - spawnedEnemies, enemiesCount);
            for (int i = 0; i < enemiesToSpawn; i++)
            {
                SpawnEnemy();
            }
        }
    }

    void SpawnEnemy()
    {
        enemiesCount--;
        spawnedEnemies++;
        var randomSpawnPosition = (Vector2)myTransform.position + UtilityTools.RandomPointInCircle(randomSpawnRadius);
        var bug = BattleManager.PoolManager.PullObject((IDEnum)enemyId, randomSpawnPosition, UtilityTools.RandomZRotation());
        var enemy = bug.GetComponent<Enemy>();
        enemy.Init(EnemyDead, playerTransform);
        enemy.Activate();
        activeEnemies.Add(enemy);
        BattleManager.EnemiesController.EnemySpawned(enemy);
    }

    void EnemyDead(Enemy enemy)
    {
        activeEnemies.Remove(enemy);
        spawnedEnemies--;
        BattleManager.EnemiesController.EnemyDied(enemy);
        if (CurrentDistanceType <= DistanceType.Far)
        {
            if (enemiesCount > 0)
            {
                SpawnEnemy();
            }
        }
    }

    public void Deactivate()
    {
        int activeEnemiesCount = activeEnemies.Count;
        for (int i = 0; i < activeEnemiesCount; i++)
        {
            activeEnemies[i].Deactivate();
        }
        activeEnemies.Clear();
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, randomSpawnRadius);
    }
#endif
}
