﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootDummy : MonoBehaviour
{
    [SerializeField] UnitData unitData;
    [SerializeField] HealthController healthController;

    private void Start()
    {
        if (healthController != null)
        {
            healthController.Init(unitData);
            healthController.OnDamage += Damaged;
        }
    }

    void Damaged(float damage)
    {
        healthController.ResetHealth();
    }
}
