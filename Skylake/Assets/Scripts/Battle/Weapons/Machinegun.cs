﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Machinegun : Weapon
{
    [SerializeField] Transform shootPoint;
    [SerializeField] IDEnum bulletId;
    Bullet shotBullet;

    protected override void Shot()
    {
        if (CurrentAmmo > 0)
        {
            base.Shot();
            shotBullet = BattleManager.PoolManager.PullObject(bulletId, shootPoint.position, shootPoint.rotation).GetComponent<Bullet>();
            shotBullet.Init();
        }
    }
}
