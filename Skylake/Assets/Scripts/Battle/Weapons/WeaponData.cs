﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TheSurface/New WeaponData")]
public class WeaponData : ScriptableObject
{
    [SerializeField] AudioClip shootSound;
    [SerializeField] IDEnum magazineID;
    [SerializeField] GameObject magazineInHandPrefab;
    [SerializeField] int maxAmmo;
    [SerializeField] float reloadTime;
    [SerializeField] float roundsPerMinute;
    [SerializeField] float shootPrepareTime;
    [SerializeField] float aimAssistDistance;
    [SerializeField] float aimAssistWidth;
    [SerializeField] float range;
    [SerializeField] float minDamage;
    [SerializeField] float maxDamage;

    public IDEnum MagazineID { get { return magazineID; } }
    public GameObject MagazineInHandPrefab { get { return magazineInHandPrefab; } }
    public int MaxAmmo { get { return maxAmmo; } }
    public float ReloadTime { get { return reloadTime; } }
    public float RoundsPerMinute { get { return roundsPerMinute; } }
    public float ShootPrepareTime { get { return shootPrepareTime; } }
    public float AimAssistDistance { get { return Mathf.Min(aimAssistDistance, range); } }
    public float AimAssistWidth { get { return aimAssistWidth; } }
    public float Range { get { return range; } }
    public float MinDamage { get { return minDamage; } }
    public float MaxDamage { get { return maxDamage; } }
}
