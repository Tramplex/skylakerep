﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] WeaponData weaponData;
    [SerializeField] Transform magazineDropPoint;
    [SerializeField] AudioSource aud;
    [SerializeField] AudioClip shotClip;
    [SerializeField] List<ParticleSystem> shotParticles;
    public event System.Action OnShotFired;
    public event System.Action OnWeaponReloaded;
    public event System.Action OnWeaponStartReload;
    public WeaponData WeaponData { get { return weaponData; } }
    public int MaxAmmo { get; protected set; }
    public int CurrentAmmo { get; protected set; }
    public float ReloadTime { get; protected set; }
    public float CooldownTime { get; protected set; }
    float shootPrepareTime;
    int shotParticlesCount;
    protected bool shooting;
    protected bool reloading;
    protected bool cooldown;
    protected Coroutine cooldownCoroutine;
    protected Coroutine reloadCoroutine;
    protected Coroutine prepareShootCoroutine;
    GameObject magazineInHand;

    protected virtual void Awake()
    {
        shotParticlesCount = shotParticles.Count;
    }

    public virtual void Init(Transform magazineInHandHolder) 
    {
        magazineInHand = Instantiate(WeaponData.MagazineInHandPrefab);
        var magazineTransform = magazineInHand.transform;
        magazineTransform.SetParent(magazineInHandHolder);
        magazineTransform.localPosition = Vector3.zero;
        magazineTransform.localRotation = Quaternion.identity;
        magazineInHand.SetActive(false);
        CurrentAmmo = MaxAmmo = weaponData.MaxAmmo;
        ReloadTime = weaponData.ReloadTime;
        shootPrepareTime = weaponData.ShootPrepareTime;
        CooldownTime = 60f / weaponData.RoundsPerMinute;
        PlayerEventManager.OnDropMagazine += DropMagazine;
        PlayerEventManager.OnShowMagazine += ShowMagazineInHand;
        PlayerEventManager.OnHideMagazine += HideMagazineInHand;
    }

    public virtual void Deactivate()
    {
        Destroy(magazineInHand);
        PlayerEventManager.OnDropMagazine -= DropMagazine;
        PlayerEventManager.OnShowMagazine -= ShowMagazineInHand;
        PlayerEventManager.OnHideMagazine -= HideMagazineInHand;
        StopShooting();
        OnShotFired = null;
    }

    void HideMagazineInHand()
    {
        magazineInHand.SetActive(false);
    }

    void ShowMagazineInHand()
    {
        magazineInHand.SetActive(true);
    }

    void DropMagazine()
    {
        BattleManager.PoolManager.PullObject(WeaponData.MagazineID, magazineDropPoint.position, magazineDropPoint.rotation);
    }

    public virtual void StartShooting()
    {
        prepareShootCoroutine = StartCoroutine(WaitPrepareShoot());
    }

    IEnumerator WaitPrepareShoot()
    {
        yield return CoroutineTools.GetYield(shootPrepareTime);
        shooting = true;
        if (!cooldown && !reloading)
        {
            Shot();
        }
    }

    public virtual void StopShooting()
    {
        CoroutineTools.StopAndNullCoroutine(ref prepareShootCoroutine, this);
        shooting = false;
    }

    protected virtual void Shot()
    {
        CurrentAmmo--;
        cooldown = true;
        cooldownCoroutine = StartCoroutine(WaitCooldown());
        aud.PlayOneShot(shotClip);
        for(int i = 0; i < shotParticlesCount; i++)
        {
            shotParticles[i].Play(false);
        }
        PlayerEventManager.Event(TypeOfEvent.ShootWeapon);
        OnShotFired?.Invoke();
        if (CurrentAmmo <= 0)
        {
            StartWeaponReload();
        }
    }

    protected virtual void WeaponReloaded()
    {
        CurrentAmmo = MaxAmmo;
        reloading = false;
        OnWeaponReloaded?.Invoke();
        if (shooting && !cooldown)
        {
            Shot();
        }
    }

    public virtual void StartWeaponReload()
    {
        if (CurrentAmmo < MaxAmmo && !reloading)
        {
            CurrentAmmo = 0;
            OnWeaponStartReload?.Invoke();
            reloading = true;
            PlayerEventManager.Event(TypeOfEvent.PlayerWeaponStartReload);
            reloadCoroutine = StartCoroutine(WaitReload());
        }
    }

    protected virtual IEnumerator WaitReload()
    {
        yield return CoroutineTools.GetYield(ReloadTime);
        WeaponReloaded();
    }

    protected virtual IEnumerator WaitCooldown()
    {
        yield return CoroutineTools.GetYield(CooldownTime);
        cooldown = false;
        cooldownCoroutine = null;
        if (shooting && !reloading)
        {
            Shot();
        }
    }
}
