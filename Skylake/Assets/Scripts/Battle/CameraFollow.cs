﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour, IUpdatable
{
    public bool SubscribedForUpdates { get; set; }
    [SerializeField] float followSpeed;
	[SerializeField] Vector3 offset;
	public static Transform target;
    Transform myTransform;

    private void Awake()
    {
        myTransform = transform;
    }

    private void OnEnable()
    {
        GameManager.UpdateManager.SubscribeForUpdate(this);
    }

    private void OnDisable()
    {
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
    }

    public void OnUpdate()
    {
        if (target != null)
        {
            myTransform.position = Vector3.Lerp(myTransform.position, target.position + offset, followSpeed * Time.deltaTime);
        }
    }
}
