﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesController : MonoBehaviour
{
    List<Enemy> allActiveEnemies = new List<Enemy>();

    public void Init()
    {
        EventManager.OnBattleEnd += BattleFinished;
    }

    public void EnemySpawned(Enemy spawnedEnemy)
    {
        allActiveEnemies.Add(spawnedEnemy);
    }

    public void EnemyDied(Enemy diedEnemy)
    {
        allActiveEnemies.Remove(diedEnemy);
    }

    public void EnemyWarning(Vector3 position, float warningRadius)
    {
        int activeEnemiesCount = allActiveEnemies.Count;
        for (int i = 0; i < activeEnemiesCount; i++)
        {
            if ((allActiveEnemies[i].myTransform.position - position).sqrMagnitude < warningRadius)
            {
                allActiveEnemies[i].SetCurrentBehaviour(EnemyState.Attacking);
            }
        }
    }

    void BattleFinished()
    {
        allActiveEnemies.Clear();
    }
}
