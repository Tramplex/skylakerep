﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CameraHeadbobController : MonoBehaviour
{
    Animator anim;
    bool isRunning = false;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        GameManager.InputManager.OnPlayerRunStart += RunStart;
        GameManager.InputManager.OnPlayerRunEnd += RunEnd;
    }

    private void Update()
    {
        anim.SetFloat(Constants.CAMERA_HEADBOB_ANIMATOR_PARAMETER, Mathf.InverseLerp(0f, isRunning ? PlayerMove.maxRunSpeed * 0.001f : PlayerMove.maxSpeed * 0.001f, PlayerMove.movedDistance));
    }

    void RunStart()
    {
        isRunning = true;
    }

    void RunEnd()
    {
        isRunning = false;
    }
}
