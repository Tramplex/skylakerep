﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipGenerator : BasicGenerator
{
    [SerializeField] float shipSize;
    CellType[,] cells;
    List<RoomBlock> roomBlocks = new List<RoomBlock>();

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            roomBlocks.Clear();
            GenerateShip();
        }
    }

    void GenerateShip()
    {
        int bridgeWidth = Mathf.RoundToInt(shipSize * Random.Range(8, 13) * 0.5f) * 2;
        int bridgeLength = Mathf.RoundToInt(shipSize * Random.Range(10, 12));
        int bridgeStartX = Mathf.CeilToInt(-bridgeWidth * 0.5f);
        int bridgeStartY = Mathf.CeilToInt(-bridgeLength * 0.5f);
        RoomBlock bridgeBlock = new RoomBlock(RoomBlockType.Rooms, bridgeStartX, bridgeStartY, bridgeWidth, bridgeLength);
        roomBlocks.Add(bridgeBlock);

        int connectionWidth = Random.Range(4, 6);
        int connectionLength = Random.Range(3, 6);
        int connectionStartX = Mathf.CeilToInt(-connectionWidth * 0.5f);
        int connectionStartY = bridgeStartY - connectionLength;
        RoomBlock connectionBlock = new RoomBlock(RoomBlockType.Connection, connectionStartX, connectionStartY, connectionWidth, connectionLength);
        roomBlocks.Add(connectionBlock);

        int mainHullWidth = Mathf.RoundToInt(shipSize * Random.Range(12, 18) * 0.5f) * 2;
        int mainHullLength = Mathf.RoundToInt(shipSize * Random.Range(25, 35));
        int mainHullStartX = Mathf.CeilToInt(-mainHullWidth * 0.5f);
        int mainHullStartY = bridgeStartY - connectionLength - mainHullLength;
        RoomBlock mainHullBlock = new RoomBlock(RoomBlockType.Rooms, mainHullStartX, mainHullStartY, mainHullWidth, mainHullLength);
        roomBlocks.Add(mainHullBlock);

        connectionWidth = Random.Range(5, 7);
        connectionLength = Mathf.Clamp(bridgeStartX - mainHullStartX, 0, 1000) + Random.Range(3, 6);
        int storageWidth = Mathf.RoundToInt(shipSize * Random.Range(10, 20) * 0.5f) * 2;
        int storageLength = Mathf.RoundToInt(shipSize * Random.Range(18, 25));
        int storageStartX = Mathf.CeilToInt(-mainHullWidth * 0.5f - connectionLength - storageWidth);
        int storageStartY = Random.Range(mainHullStartY - Mathf.RoundToInt(storageLength * 0.6f), mainHullStartY + mainHullLength - Mathf.RoundToInt(storageLength * 0.4f));
        RoomBlock storageBlock = new RoomBlock(RoomBlockType.Rooms, storageStartX, storageStartY, storageWidth, storageLength);
        roomBlocks.Add(storageBlock);
        storageStartX = Mathf.CeilToInt(mainHullWidth * 0.5f + connectionLength);
        storageBlock = new RoomBlock(RoomBlockType.Rooms, storageStartX, storageStartY, storageWidth, storageLength);
        roomBlocks.Add(storageBlock);

        int conStartY = Mathf.Max(mainHullStartY, storageStartY);
        int conEndY = Mathf.Min(mainHullStartY + mainHullLength, storageStartY + storageLength);
        int connectedCount = conEndY - conStartY;
        int maxConnectionsCount = Mathf.FloorToInt(connectedCount / (connectionWidth + 2));
        int connectionsAmount = Random.Range(Mathf.Clamp(Mathf.FloorToInt(maxConnectionsCount * 0.5f), 1, maxConnectionsCount), maxConnectionsCount);
        int wiggleRoom = ((maxConnectionsCount - connectionsAmount) * connectionWidth) / connectionsAmount;
        
        connectionStartX = mainHullStartX - connectionLength;
        int lastConEndY = conStartY;
        for (int i = 0; i < connectionsAmount; i++)
        {
            connectionStartY = Mathf.RoundToInt(lastConEndY + 2 + Random.Range(0, wiggleRoom));
            lastConEndY = connectionStartY + connectionWidth;
            connectionBlock = new RoomBlock(RoomBlockType.Connection, connectionStartX, connectionStartY, connectionLength, connectionWidth);
            roomBlocks.Add(connectionBlock);
        }

        connectionLength = Random.Range(5, 9);
        int enginesWidth = Mathf.RoundToInt(shipSize * Random.Range(7, 13) * 0.5f) * 2;
        int enginesLength = Mathf.RoundToInt(shipSize * Random.Range(10, 25));
        int enginesStartX = Mathf.CeilToInt(-enginesWidth * 0.5f);
        int enginesStartY = mainHullStartY - connectionLength - enginesLength;
        RoomBlock enginesHullBlock = new RoomBlock(RoomBlockType.Rooms, enginesStartX, enginesStartY, enginesWidth, enginesLength);
        roomBlocks.Add(enginesHullBlock);

        connectionWidth = Random.Range(6, 8);
        connectionStartX = Mathf.CeilToInt(-connectionWidth * 0.5f);
        connectionStartY = mainHullStartY - connectionLength;
        connectionBlock = new RoomBlock(RoomBlockType.Connection, connectionStartX, connectionStartY, connectionWidth, connectionLength);
        roomBlocks.Add(connectionBlock);
    }
}
