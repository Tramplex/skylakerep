﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicGenerator : MonoBehaviour
{
    protected List<RoomBlock> roomBlocks = new List<RoomBlock>();

    protected List<RoomBlock> CreateConnectionsAlongLine(bool vertical, int conFrom, int conTo, int connectionWidth)
    {
        List<RoomBlock> result = new List<RoomBlock>();
        //if (vertical)
        //{

        //}
        //else
        //{
        //    int connectedCount = conTo - conFrom;
        //    int maxConnectionsCount = Mathf.FloorToInt(connectedCount / (connectionWidth + 2));
        //    int connectionsAmount = Random.Range(Mathf.Clamp(Mathf.FloorToInt(maxConnectionsCount * 0.5f), 1, maxConnectionsCount), maxConnectionsCount);
        //    int wiggleRoom = (maxConnectionsCount - connectionsAmount) * connectionWidth;
        //    connectionStartX = mainHullStartX - connectionLength;
        //    int lastConEndY = conStartY;
        //    for (int i = 0; i < connectionsAmount; i++)
        //    {
        //        connectionStartY = Mathf.RoundToInt(lastConEndY + 2 + Random.Range(0, wiggleRoom));
        //        lastConEndY = connectionStartY + connectionWidth;
        //        connectionBlock = new RoomBlock(connectionStartX, connectionStartY, connectionLength, connectionWidth);
        //        roomBlocks.Add(connectionBlock);
        //    }
        //}
        return result;
    }

    public class RoomBlock
    {
        public RoomBlockType type;
        public int xStart;
        public int xEnd;
        public int yStart;
        public int yEnd;
        public int width;
        public int length;

        public RoomBlock(RoomBlockType type, int xStart, int yStart, int width, int length)
        {
            this.type = type;
            this.xStart = xStart;
            this.yStart = yStart;
            this.width = width;
            this.length = length;
            xEnd = xStart + width;
            yEnd = yStart + length;
        }
    }

    private void OnDrawGizmos()
    {
        if (roomBlocks.Count == 0) return;
        for (int i = 0; i < roomBlocks.Count; i++)
        {
            switch (roomBlocks[i].type)
            {
                case RoomBlockType.Connection:
                    Gizmos.color = Color.yellow;
                    break;
                case RoomBlockType.Rooms:
                    Gizmos.color = Color.green;
                    break;
            }
            int xStart = roomBlocks[i].xStart;
            int yStart = roomBlocks[i].yStart;
            int xEnd = roomBlocks[i].xStart + roomBlocks[i].width;
            int yEnd = roomBlocks[i].yStart + roomBlocks[i].length;
            Vector3 size = Vector3.one * 0.4f;
            for (int x = xStart; x < xEnd; x++)
            {
                for (int y = yStart; y < yEnd; y++)
                {
                    Vector3 cellCenter = new Vector3(x, y);
                    Gizmos.DrawWireCube(cellCenter, size);
                }
            }
        }
    }
}

public enum CellType
{
    Floor,
    Wall,
}

public enum RoomBlockType
{
    Connection,
    Rooms,
}
