﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapManager : MonoBehaviour
{
    public static TilemapManager Instance;
    [SerializeField] List<Tilemap> tilemaps = new List<Tilemap>();
    [SerializeField] List<TileBase> tiles = new List<TileBase>();

    private void Awake()
    {
        Instance = this;
    }

    public void InitTilemaps(Vector3Int origin, Vector3Int size)
    {
        //size = new Vector3Int(200, 200, 0);
        for(int i = 0; i < tilemaps.Count; i++)
        {
            tilemaps[i].size = size;
            tilemaps[i].origin = origin;
        }
        GetTilemap(TilemapType.Ground).BoxFill( origin, GetTile(TileType.Walls), origin.x, origin.y, origin.x + size.x, origin.y + size.y);
    }

    public void RefreshTiles()
    {
        for (int i = 0; i < tilemaps.Count; i++)
        {
            tilemaps[i].RefreshAllTiles();
        }
    }

    public void SetTilesOnTilemap(TilemapType tilemapType, Vector3[] positions, TileType type)
    {
        var tile = GetTile(type);
        var tilemap = GetTilemap(tilemapType);
        //for(int i = 0; i < )
        //tilemap.SetTiles(positions, );
    }

    public Tilemap GetTilemap(TilemapType type)
    {
        int typeInt = (int)type;
        if (typeInt < 0 || typeInt >= tilemaps.Count)
        {
            Debug.Log($"There is no tilemap with type {type}");
            return null;
        }
        else return tilemaps[typeInt];
    }

    public TileBase GetTile(TileType type)
    {
        int typeInt = (int)type;
        if (typeInt < 0 || typeInt >= tiles.Count)
        {
            Debug.Log($"There is no tile with type {type}");
            return null;
        }
        else return tiles[typeInt];
    }
}

public enum TilemapType
{
    Ground,
    Walls,
}

public enum TileType
{
    Ground,
    Walls,
}
