﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Room
{
    public Color roomColor;
    public List<MapCell> roomCells = new List<MapCell>();
    List<MapCell> freeSpaces = new List<MapCell>();
    MapCell[,] mapCells;

    public Room(List<MapCell> roomCells)
    {
        roomColor = Random.ColorHSV();
        this.roomCells = roomCells;
        mapCells = StationGenerator.mapCells;
        for (int i = 0; i < roomCells.Count; i++)
        {
            roomCells[i].assignedRoom = this;
        }
        UpdateFreeSpaces();
    }

    void UpdateFreeSpaces()
    {
        freeSpaces.Clear();
        for (int i = 0; i < roomCells.Count; i++)
        {
            if (roomCells[i].X > 0)
            {
                var cell = mapCells[roomCells[i].X - 1, roomCells[i].Y];
                if (cell.assignedRoom == null) freeSpaces.Add(cell);
            }
            if (roomCells[i].X < StationGenerator.loadedRadius)
            {
                var cell = mapCells[roomCells[i].X + 1, roomCells[i].Y];
                if (cell.assignedRoom == null) freeSpaces.Add(cell);
            }
            if (roomCells[i].Y > 0)
            {
                var cell = mapCells[roomCells[i].X, roomCells[i].Y - 1];
                if (cell.assignedRoom == null) freeSpaces.Add(cell);
            }
            if (roomCells[i].Y < StationGenerator.loadedRadius)
            {
                var cell = mapCells[roomCells[i].X, roomCells[i].Y + 1];
                if (cell.assignedRoom == null) freeSpaces.Add(cell);
            }
        }
    }

    int stopper = 0;
    public List<Room> GenerateNeighbors()
    {
        stopper = 0;
        List<Room> result = new List<Room>();
        while (freeSpaces.Count > 0 && stopper < 100)
        {
            MapCell newRoomStartCell = freeSpaces[Random.Range(0, freeSpaces.Count)];
            int newRoomVolume = Random.Range(7, 15);
            List<MapCell> newRoomCells = new List<MapCell>() { newRoomStartCell };
            List<MapCell> temp = new List<MapCell>();
            int newRoomStopper = 0;
            while (newRoomCells.Count < newRoomVolume && newRoomStopper < 100)
            {
                temp.Clear();
                var direction = MathfTools.GetRandomDirection();
                for (int i = 0; i < newRoomCells.Count; i++)
                {
                    var cell = mapCells[newRoomCells[i].X + (int)direction.x, newRoomCells[i].Y + (int)direction.y];
                    if (cell.assignedRoom == null && !newRoomCells.Contains(cell) && !temp.Contains(cell)) temp.Add(cell);
                }
                newRoomCells.AddRange(temp);
                newRoomStopper++;
            }
            if (newRoomCells.Count > 0) result.Add(new Room(newRoomCells));
            UpdateFreeSpaces();
            stopper++;
            if (stopper >= 100) Debug.Log("Shit");
        }
        return result;
    }

    public void GenerateTiles()
    {
        var groundTilemap = TilemapManager.Instance.GetTilemap(TilemapType.Ground);
        for (int i = 0; i < roomCells.Count; i++)
        {
            int xStart = roomCells[i].X * MapCell.CellDiameter - MapCell.CellRadius;
            int xEnd = roomCells[i].X * MapCell.CellDiameter + MapCell.CellRadius - 1;
            int yStart = roomCells[i].Y * MapCell.CellDiameter - MapCell.CellRadius;
            int yEnd = roomCells[i].Y * MapCell.CellDiameter + MapCell.CellRadius - 1;
            groundTilemap.BoxFill(new Vector3Int(xStart, yStart, 0), TilemapManager.Instance.GetTile(TileType.Ground)
                , xStart, yStart, xEnd, yEnd);
        }
    }

    public void GenerateWalls()
    {
        var tilemap = TilemapManager.Instance.GetTilemap(TilemapType.Ground);
        var wallTile = TilemapManager.Instance.GetTile(TileType.Walls);
        List<Vector3Int> positions = new List<Vector3Int>();
        for (int i = 0; i < roomCells.Count; i++)
        {
            int X = roomCells[i].X;
            int Y = roomCells[i].Y;
            if (mapCells[X, Y + 1].assignedRoom != this)
            {
                int xStart = X * MapCell.CellDiameter - MapCell.CellRadius;
                int xEnd = xStart + MapCell.CellDiameter;
                int yPos = Y * MapCell.CellDiameter + MapCell.CellRadius - 1;
                for (int p = xStart; p < xEnd; p++)
                {
                    positions.Add(new Vector3Int(p, yPos, 0));
                }
            }
            if (mapCells[X, Y - 1].assignedRoom != this)
            {
                int xStart = X * MapCell.CellDiameter - MapCell.CellRadius+1;
                int xEnd = xStart + MapCell.CellDiameter;
                int yPos = Y * MapCell.CellDiameter - MapCell.CellRadius;
                for (int p = xStart; p < xEnd; p++)
                {
                    positions.Add(new Vector3Int(p, yPos, 0));
                }
            }
            if (mapCells[X + 1, Y].assignedRoom != this)
            {
                int xStart = X * MapCell.CellDiameter + MapCell.CellRadius - 1;
                int yStart = Y * MapCell.CellDiameter - MapCell.CellRadius;
                int yEnd = yStart + MapCell.CellDiameter;
                for (int p = yStart; p < yEnd; p++)
                {
                    positions.Add(new Vector3Int(xStart, p, 0));
                }
            }
            if (mapCells[X - 1, Y].assignedRoom != this)
            {
                int xStart = X * MapCell.CellDiameter - MapCell.CellRadius;
                int yStart = Y * MapCell.CellDiameter - MapCell.CellRadius;
                int yEnd = yStart + MapCell.CellDiameter;
                for (int p = yStart; p < yEnd; p++)
                {
                    positions.Add(new Vector3Int(xStart, p, 0));
                }
            }
        }
        TileBase[] tiles = new TileBase[positions.Count];
        for (int p = 0; p < positions.Count; p++)
        {
            tiles[p] = wallTile;
        }
        tilemap.SetTiles(positions.ToArray(), tiles);
    }
}
