﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class StationGenerator : MonoBehaviour
{
    [SerializeField] int loadRadius;
    public static int loadedRadius;
    public static MapCell[,] mapCells;
    List<Room> rooms = new List<Room>();

    private void Start()
    {
        loadedRadius = loadRadius;
        mapCells = new MapCell[loadedRadius, loadedRadius];
        PopulateCells();
        GenerateStation();
    }

    void GenerateStation()
    {
        TilemapManager.Instance.InitTilemaps(new Vector3Int(0, 0, 0), new Vector3Int(loadedRadius * MapCell.CellDiameter, loadedRadius * MapCell.CellDiameter, 0));
        int startRoomWidth = 4;
        int startRoomLength = 6;
        int startX = (int)(loadedRadius * 0.5f) - (int)(startRoomWidth * 0.5f);
        int startY = (int)(loadedRadius * 0.5f) - (int)(startRoomLength * 0.5f);
        int endX = (int)(loadedRadius * 0.5f) + (int)(startRoomWidth * 0.5f);
        int endY = (int)(loadedRadius * 0.5f) + (int)(startRoomLength * 0.5f);
        List<MapCell> startRoomCells = new List<MapCell>();
        for (int x = startX; x <= endX; x++)
        {
            for (int y = startY; y <= endY; y++)
            {
                startRoomCells.Add(mapCells[x, y]);
            }
        }
        var startingRoom = new Room(startRoomCells);
        rooms.Add(startingRoom);
        var newRooms = startingRoom.GenerateNeighbors();
        rooms.AddRange(newRooms);
        for(int i = 0; i < rooms.Count; i++)
        {
            rooms[i].GenerateTiles();
        }
        for(int i = 0; i < rooms.Count; i++)
        {
            rooms[i].GenerateWalls();
        }
        TilemapManager.Instance.RefreshTiles();
    }

    void PopulateCells()
    {
        for (int x = 0; x < loadedRadius; x++)
        {
            for (int y = 0; y < loadedRadius; y++)
            {
                mapCells[x, y] = new MapCell(x, y);
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (rooms.Count == 0) return;
        Vector3 size = Vector3.one * MapCell.CellRadius * 2 * 0.9f;
        for (int i = 0; i < rooms.Count; i++)
        {
            Gizmos.color = rooms[i].roomColor;
            for(int r = 0; r < rooms[i].roomCells.Count; r++)
            {
                Vector3 position = new Vector3(rooms[i].roomCells[r].X * MapCell.CellDiameter, rooms[i].roomCells[r].Y * MapCell.CellDiameter, 0f);
                Gizmos.DrawWireCube(position, size);
                Handles.Label(position, $"{rooms[i].roomCells[r].X},{rooms[i].roomCells[r].Y}");
            }
        }
    }
}

public class MapCell
{
    public int X;
    public int Y;
    public static int CellRadius = 3;
    public static int CellDiameter { get { return CellRadius * 2; } }
    public Room assignedRoom;

    public MapCell(int x, int y)
    {
        X = x;
        Y = y;
    }
}


