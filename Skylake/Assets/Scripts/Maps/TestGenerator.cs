﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGenerator : BasicGenerator
{
    private void Start()
    {
        TestGenerate();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            roomBlocks.Clear();
            TestGenerate();
        }
    }

    void TestGenerate()
    {
        RoomBlock firstBlock = new RoomBlock(RoomBlockType.Rooms, 0, 3, 20, 20);
        RoomBlock secondBlock = new RoomBlock(RoomBlockType.Rooms, 0, -23, 20, 20);
        roomBlocks.Add(firstBlock);
        roomBlocks.Add(secondBlock);

        var checkPoints1 = new List<Vector2>();
        checkPoints1.Add(new Vector2(firstBlock.xStart, firstBlock.yStart));
        checkPoints1.Add(new Vector2(firstBlock.xEnd, firstBlock.yStart));
        checkPoints1.Add(new Vector2(firstBlock.xStart, firstBlock.yEnd));
        checkPoints1.Add(new Vector2(firstBlock.xEnd, firstBlock.yEnd));
        var checkPoints2 = new List<Vector2>();
        checkPoints2.Add(new Vector2(secondBlock.xStart, secondBlock.yStart));
        checkPoints2.Add(new Vector2(secondBlock.xEnd, secondBlock.yStart));
        checkPoints2.Add(new Vector2(secondBlock.xStart, secondBlock.yEnd));
        checkPoints2.Add(new Vector2(secondBlock.xEnd, secondBlock.yEnd));
        Vector2 closestPoint1 = checkPoints1[0];
        Vector2 closestPoint2 = checkPoints2[0];
        float temp = float.PositiveInfinity;
        for(int i = 0; i < checkPoints1.Count; i++)
        {
            for (int k = 0; k < checkPoints2.Count; k++)
            {
                var distance = (checkPoints1[i] - checkPoints2[k]).sqrMagnitude;
                if(distance < temp)
                {
                    temp = distance;
                    closestPoint1 = checkPoints1[i];
                    closestPoint2 = checkPoints2[k];
                }
            }
        }


    }
}
