﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour
{
    public static Constants Instance;
    private void Awake()
    {
        Instance = this;
    }

    #region Settings
    public const float LOW_AMMO_THRESHOLD = 0.3f;
    public const int MIN_ROOM_SIZE = 4;
    public const int MAX_ROOM_SIZE = 10;
    #endregion

    public const string MENU_SCENE_NAME = "MenuScene";
    public const string GAME_SCENE_NAME = "GameScene";
    public const string HUB_SCENE_NAME = "HubScene";
    public const string MAIN_CANVAS_TAG = "MainCanvas";
    public const string FIRST_LAUNCH_STRING = "FirstLaunch";
    public const string AUTO_RELOAD = "AutoReload";
    public const string PLAYER_TRANSFORM = "playerTransform";
    public const string STATE_DATA = "stateData";
    public const string OVER_EVERYTHING_SORTING_LAYER = "OverEverything";
    public const string POOL_TRANSFORM_NAME = "PoolTransform";
    public const string CAMERA_HEADBOB_ANIMATOR_PARAMETER = "Headbob";
    public const float AUTO_RELOAD_TIME = 1f;
    public const float CHECK_DESTINATION_IN_VIEW_COOLDOWN = 0.2f;
    public const float FIND_NEW_PATH_COOLDOWN = 1f;

    #region Layer masks
    public const int ENEMY_LAYER_MASK = (1 << 8);
    public const int OBSTACLE_LAYER_MASK = (1 << 9);
    public const int PLAYER_LAYER_MASK = (1 << 10);
    public const int BULLET_LAYER_MASK = (1 << 11);
    #endregion

    #region Gamepad controls
    public const string leftHorizontalAxis = "LeftHorizontal";
    public const string leftVerticalAxis = "LeftVertical";
    public const string rightHorizontalAxis = "RightHorizontal";
    public const string rightVerticalAxis = "RightVertical";
    public const string crossHorizontalAxis = "CrossHorizontal";
    public const string crossVerticalAxis = "CrossVertical";
    public const string aButton = "AButton";
    public const string bButton = "BButton";
    public const string xButton = "XButton";
    public const string yButton = "YButton";
    #endregion

    static List<int> animationsHash = new List<int>() { Animator.StringToHash("Idle"), Animator.StringToHash("Attack"), Animator.StringToHash("Move"), Animator.StringToHash("Reload"), Animator.StringToHash("ReloadSpeed") };
    public static int GetAnimationHash(AnimationEnum animation)
    {
        return animationsHash[(int)animation];
    }
}

public enum IDEnum
{
    NotDefined = 0,
    PlayerID,
    MachinegunID,
    BugID,
    MachineGunBulletID,
    MachineGunBulletHitID, // 5
    BugDeployEffectID,
    BugBulletHitEffect,
    BloodSplat_1,
    GroundBloodSplat_1,
    PistolMagazine, // 10
    LureID,
}

public enum EnemyType
{
    Bug = IDEnum.BugID,
    Lure = IDEnum.LureID,
}

public enum EnemyState
{
    Idle,
    MovingToPosition,
    Attacking,
    ChasingTarget
}

// Dont forget to add new ones to animation hashes
public enum AnimationEnum
{
    Idle = 0,
    Attack,
    Move,
    Reload,
    ReloadSpeed,
}

public enum EnemyAttackType
{
    NotDefined,
    SimpleMelee,
}

public enum FactionType
{
    Player,
    Enemy
}

public enum SceneNamesEnum
{
    MenuScene,
    GameScene,
    HubScene,
}
