﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Physics2DExtentions
{
    static float maxMidRayInterval = 0.2f;

    /// <summary>
    /// End width must be bigger than start width
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="direction"></param>
    /// <param name="distance"></param>
    /// <param name="startWidth"></param>
    /// <param name="endWidth"></param>
    public static ConeHitResult ConeCast2D(Vector2 origin, Vector2 direction, float distance, float startWidth, float endWidth)
    {
        RaycastHit2D closestHit = new RaycastHit2D();
        float closestHitSqrDistance = distance * distance;
        RaycastHit2D[] hits = new RaycastHit2D[10];
        int raysCount = Mathf.CeilToInt(endWidth / maxMidRayInterval);
        int halfRayCount = Mathf.FloorToInt(raysCount / 2f);
        Vector2 leftDirection = Vector2.Perpendicular(direction).normalized;
        float rayStartSpacing = startWidth / (raysCount - 1);
        float rayEndSpacing = endWidth / (raysCount - 1);
        int hitsCount = 0;
        for (int i = 0; i < raysCount; i++)
        {
            var rayOrigin = origin + (i - halfRayCount) * leftDirection * rayStartSpacing;
            var rayDirection = direction * distance + (i - halfRayCount) * leftDirection * rayEndSpacing;
            var rayDistance = Vector2.Distance(rayOrigin, rayDirection + origin);
            hitsCount = Physics2D.RaycastNonAlloc(rayOrigin, rayDirection, hits, rayDistance, Constants.ENEMY_LAYER_MASK);
            if (hitsCount > 0)
            {
                for (int h = 0; h < hitsCount; h++)
                {
                    float sqrDistanceToHit = (origin - hits[h].point).sqrMagnitude;
                    if (sqrDistanceToHit < closestHitSqrDistance)
                    {
                        closestHitSqrDistance = sqrDistanceToHit;
                        closestHit = hits[h];
                    }
                }
            }
            //Debug.DrawRay(rayOrigin, rayDirection * rayDistance, Color.red, 1000);
        }
        return new ConeHitResult(closestHit, closestHit.collider != null);
    }
}

public class ConeHitResult
{
    public RaycastHit2D hit;
    public bool hitSomething = false;

    public ConeHitResult(RaycastHit2D hit, bool hitSomething)
    {
        this.hit = hit;
        this.hitSomething = hitSomething;
    }
}
