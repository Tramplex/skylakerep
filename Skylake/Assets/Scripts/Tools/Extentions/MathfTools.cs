﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathfTools
{
    public static Vector2 GetRandomDirection()
    {
        Vector2 result = Vector2.zero;
        int xOry = Random.Range(0, 2);
        if(xOry == 0) result.x = Random.Range(0, 2) == 0 ? 1 : -1;
        else result.y = Random.Range(0, 2) == 0 ? 1 : -1;
        return result;
    }
}
