﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceChecker : MonoBehaviour
{
    // All thresholds are squared
    public static float closeToFarThreshold = 400;
    public static float farToVeryFarThreshold = 900;
    static List<IDistanceChecker> checkItems = new List<IDistanceChecker>();
    static Transform playerTransform;

    public void Init(Transform plTransform)
    {
        playerTransform = plTransform;
    }

    static float tempDistance;
    private void Update()
    {
        if (playerTransform == null) return;
        for (int i = 0; i < checkItems.Count; i++)
        {
            switch (checkItems[i].CurrentDistanceType)
            {
                case DistanceType.Close:
                    if ((checkItems[i].CheckTransform.position - playerTransform.position).sqrMagnitude > closeToFarThreshold)
                    {
                        checkItems[i].CurrentDistanceType = DistanceType.Far;
                        checkItems[i].OnDistanceChange(DistanceType.Far);
                    }
                    break;
                case DistanceType.Far:
                    tempDistance = (checkItems[i].CheckTransform.position - playerTransform.position).sqrMagnitude;
                    if (tempDistance < closeToFarThreshold)
                    {
                        checkItems[i].CurrentDistanceType = DistanceType.Close;
                        checkItems[i].OnDistanceChange(DistanceType.Close);
                    }
                    else if (tempDistance > farToVeryFarThreshold)
                    {
                        checkItems[i].CurrentDistanceType = DistanceType.VeryFar;
                        checkItems[i].OnDistanceChange(DistanceType.VeryFar);
                    }
                    break;
                case DistanceType.VeryFar:
                    if ((checkItems[i].CheckTransform.position - playerTransform.position).sqrMagnitude < farToVeryFarThreshold)
                    {
                        checkItems[i].CurrentDistanceType = DistanceType.Far;
                        checkItems[i].OnDistanceChange(DistanceType.Far);
                    }
                    break;
            }
        }
    }

    public void ResetCheck()
    {
        for (int i = 0; i < checkItems.Count; i++)
        {
            if(checkItems[i] != null)
            {
                checkItems[i].IsChecked = false;
            }
        }
        checkItems.Clear();
    }

    public static void AddChecker(IDistanceChecker checkItem)
    {
        if (!checkItem.IsChecked)
        {
            tempDistance = (checkItem.CheckTransform.position - playerTransform.position).sqrMagnitude;
            if (tempDistance < closeToFarThreshold) 
                checkItem.CurrentDistanceType = DistanceType.Close;
            else if (tempDistance >= closeToFarThreshold && tempDistance < farToVeryFarThreshold) 
                checkItem.CurrentDistanceType = DistanceType.Far;
            else if (tempDistance >= farToVeryFarThreshold) 
                checkItem.CurrentDistanceType = DistanceType.VeryFar;
            checkItem.OnDistanceChange(checkItem.CurrentDistanceType);
            checkItems.Add(checkItem);
            checkItem.IsChecked = true;
        }
    }

    public void RemoveChecker(IDistanceChecker checkItem)
    {
        if (checkItem.IsChecked)
        {
            checkItems.Remove(checkItem);
            checkItem.IsChecked = false;
        }
    }
}

public interface IDistanceChecker
{
    Transform CheckTransform { get; }
    bool IsChecked { get; set; }
    DistanceType CurrentDistanceType { get; set; }
    void OnDistanceChange(DistanceType newDistance);
}

public enum DistanceType
{
    Close,
    Far, // 15 units
    VeryFar, // 25 units
}
