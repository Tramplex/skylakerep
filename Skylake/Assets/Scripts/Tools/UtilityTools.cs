﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilityTools : MonoBehaviour
{
    public static Color LerpColor(ref Color a, ref Color b, float t)
    {
        if (t > 1)
        {
            t = 1;
        }
        else if (t < 0)
        {
            t = 0;
        }
        return new Color(
            a.r + (b.r - a.r) * t,
            a.g + (b.g - a.g) * t,
            a.b + (b.b - a.b) * t,
            a.a + (b.a - a.a) * t
        );
    }

    public static int FindClosestNode(List<Vector3> path, Vector3 position)
    {
        int result = 0;
        int pathLength = path.Count;
        if (pathLength == 1) return result;
        float closestNodeDistance = (position - path[0]).sqrMagnitude;
        float tempDistance;
        for (int i = 1; i < pathLength; i++)
        {
            tempDistance = (path[i] - position).sqrMagnitude;
            if (tempDistance < closestNodeDistance)
            {
                result = i;
                closestNodeDistance = tempDistance;
            }
        }
        return result;
    }

    public static Quaternion GetZRotationFromDirection(ref Vector3 direction)
    {
        return Quaternion.Euler(0f, 0f, -Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg);
    }

    public static Quaternion GetRandomZRotationFromDirection(ref Vector3 direction, float randomAngle)
    {
        return Quaternion.Euler(0f, 0f, -Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg + Random.Range(-randomAngle, randomAngle));
    }

    public static float RefSquareDistance(ref Vector3 a, ref Vector3 b)
    {
        float dx = a.x - b.x;
        float dy = a.y - b.y;
        float dz = a.z - b.z;
        return (dx * dx + dy * dy + dz * dz);
    }

    public static float RefDistance(ref Vector3 a, ref Vector3 b)
    {
        float dx = a.x - b.x;
        float dy = a.y - b.y;
        float dz = a.z - b.z;
        return Mathf.Sqrt(dx * dx + dy * dy + dz * dz);
    }

    public static Quaternion RandomZRotation(float from, float to)
    {
        return Quaternion.Euler(0f, 0f, Random.Range(from, to));
    }

    public static Quaternion RandomZRotation()
    {
        return Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
    }

    public static Vector2 RandomPointInCircle(float radius)
    {
        return Random.insideUnitCircle.normalized * Random.Range(0f, radius);
    }
}
