﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class IKTwoBoneLimb : MonoBehaviour
{
    [SerializeField] IKBone firstBone;
    Transform firstBoneTransform;
    [SerializeField] IKBone secondBone;
    Transform secondBoneTransform;
    [SerializeField] Transform target;
    [SerializeField] bool flip = false;
    List<IKBone> bones = new List<IKBone>();
    int chainLength = 2;
    Vector3 lastTargetPosition;
    bool recordNextUpdate = false;

    private void Update()
    {
        if (Application.isPlaying) return;
        IKWork();
    }

    void IKWork()
    {
        if (firstBone == null) return;
        if (secondBone == null) return;
        if (firstBoneTransform == null) firstBoneTransform = firstBone.transform;
        if (secondBoneTransform == null) secondBoneTransform = secondBone.transform;
        if (recordNextUpdate)
        {
            recordNextUpdate = false;
            Undo.RecordObjects(new Object[] { firstBoneTransform, secondBoneTransform }, "IK anim record");
        }
        firstBoneTransform.up = target.position - firstBoneTransform.position;
        secondBoneTransform.localRotation = Quaternion.identity;
        float distanceToTarget = Vector2.Distance(firstBoneTransform.position, target.position);
        if (distanceToTarget < firstBone.scaledLength + secondBone.scaledLength)
        {
            float firstBoneRotation = firstBoneTransform.rotation.eulerAngles.z + (flip ? -1 : 1) * Mathf.Acos((firstBone.scaledLength * firstBone.scaledLength + distanceToTarget * distanceToTarget - secondBone.scaledLength * secondBone.scaledLength) / (2 * firstBone.scaledLength * distanceToTarget)) * Mathf.Rad2Deg;
            float secondBoneRotation = Mathf.Acos((firstBone.scaledLength * firstBone.scaledLength + secondBone.scaledLength * secondBone.scaledLength
                - distanceToTarget * distanceToTarget) / (2 * firstBone.scaledLength * secondBone.scaledLength)) * Mathf.Rad2Deg;
            firstBoneTransform.rotation = Quaternion.Euler(0f, 0f, firstBoneRotation);
            secondBoneTransform.localRotation = Quaternion.Euler(0f, 0f, (flip ? -1 : 1) * (secondBoneRotation - 180));
        }
        lastTargetPosition = target.position;
    }

    public void RecordIKAnimation()
    {
        recordNextUpdate = true;
    }

    [MenuItem("MyMenu/Record IK in animation &q")]
    static void RecordAllIK()
    {
        if (Application.isPlaying) return;
        var IKs = FindObjectsOfType<IKTwoBoneLimb>();
        for(int i = 0;i < IKs.Length; i++)
        {
            if (IKs[i].gameObject.activeInHierarchy)
            {
                IKs[i].RecordIKAnimation();
            }
        }
    }
}
