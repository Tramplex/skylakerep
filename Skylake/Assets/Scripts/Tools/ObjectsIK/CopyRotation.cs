﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CopyRotation : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] bool localRotation;

    private void Update()
    {
        if (target == null) return;
        transform.rotation = localRotation ? target.localRotation : target.rotation;
    }
}
