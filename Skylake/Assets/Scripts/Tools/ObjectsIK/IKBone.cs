﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class IKBone : MonoBehaviour
{
    [SerializeField] Transform endPoint;
    [SerializeField] bool updateLength = false;
    [SerializeField] float length;
    public float scaledLength { get { return length * transform.lossyScale.z; } }

    private void OnEnable()
    {
        //UpdateLength();
    }

    private void Update()
    {
        if (Application.isPlaying) return;
        if (length == 0 && updateLength) UpdateLength();
    }

    [ContextMenu("Calculate length")]
    void UpdateLength()
    {
        if (endPoint == null) return;
        length = Vector2.Distance(transform.position, endPoint.position) / transform.lossyScale.z;
    }
}
