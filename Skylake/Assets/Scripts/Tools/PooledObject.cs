﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledObject : MonoBehaviour, IUpdatable
{
    public bool SubscribedForUpdates { get; set; }
    [SerializeField] bool autoRemoveTime;
    [SerializeField] protected float removeDelay;
    [HideInInspector] public int id;
    protected PoolManager poolManager;
    float removeTimer;
    bool remove = false;

    public void Init(PoolManager poolManager, ref System.Action OnPoolClear, int id)
    {
        OnPoolClear += Deactivate;
        this.poolManager = poolManager;
        this.id = id;
    }

    public virtual void OnPull()
	{
		gameObject.SetActive(true);
        removeTimer = 0;
        remove = autoRemoveTime;
        GameManager.UpdateManager.SubscribeForUpdate(this);
	}

	public virtual void OnPush()
	{
		gameObject.SetActive(false);
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
    }

    public void PushToPool()
    {
        if (removeDelay > 0)
        {
            remove = true;
        }
        else
        {
            poolManager.PushObject(this);
        }
    }

    void Deactivate()
    {
        GameManager.UpdateManager.UnsubscribeFromUpdate(this);
    }

    public virtual void OnUpdate()
    {
        if (remove)
        {
            removeTimer += Time.deltaTime;
            if (removeTimer > removeDelay)
            {
                poolManager.PushObject(this);
            }
        }
    }
}
