﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineTools : MonoBehaviour
{
    public static WaitForSeconds WaitSecond_1 = new WaitForSeconds(1f);
    public static WaitForSeconds WaitSecond_2 = new WaitForSeconds(2f);
    public static WaitForSeconds WaitSecond_3 = new WaitForSeconds(3f);
    public static WaitForSeconds WaitSecond_4 = new WaitForSeconds(4f);
    public static WaitForSeconds WaitSecond_5 = new WaitForSeconds(5f);
    public static WaitForSeconds WaitSecond_7 = new WaitForSeconds(7f);
    public static WaitForSeconds WaitSecond_10 = new WaitForSeconds(10f);
    public static WaitForSeconds WaitSecond_0_1 = new WaitForSeconds(0.1f);
    public static WaitForSeconds WaitSecond_0_2 = new WaitForSeconds(0.2f);
    public static WaitForSeconds WaitSecond_0_5 = new WaitForSeconds(0.5f);
    public static WaitForSeconds WaitSecond_0_05 = new WaitForSeconds(0.05f);
    public static WaitForSeconds WaitSecond_0_01 = new WaitForSeconds(0.01f);
    public static WaitForEndOfFrame WaitEndFrame = new WaitForEndOfFrame();

    static Dictionary<float, WaitForSeconds> yieldCache = new Dictionary<float, WaitForSeconds>();
    static Dictionary<float, WaitForSecondsRealtime> realtimeYieldCache = new Dictionary<float, WaitForSecondsRealtime>();

    public Coroutine WaitAndInvoke(System.Action action, YieldInstruction delay)
    {
        return StartCoroutine(WaitAndInvokeCoroutine(action, delay));
    }

    public Coroutine WaitAndInvoke(System.Action action, float timeToWait)
    {
        return StartCoroutine(WaitAndInvokeCoroutine(action, GetYield(timeToWait)));
    }

    public static void StopAndNullCoroutine(ref Coroutine coroutineToStop, MonoBehaviour requester)
    {
        if (coroutineToStop != null)
        {
            requester.StopCoroutine(coroutineToStop);
            coroutineToStop = null;
        }
    }

    public static WaitForSeconds GetYield(float timeToWait)
    {
        WaitForSeconds yieldInstruction = null;
        if (yieldCache.ContainsKey(timeToWait))
        {
            yieldInstruction = yieldCache[timeToWait];
        }
        else
        {
            yieldInstruction = new WaitForSeconds(timeToWait);
            yieldCache.Add(timeToWait, yieldInstruction);
        }
        return yieldInstruction;
    }

    public static WaitForSecondsRealtime GetRealtimeYield(float timeToWait)
    {
        WaitForSecondsRealtime yieldInstruction = null;
        if (realtimeYieldCache.ContainsKey(timeToWait))
        {
            yieldInstruction = realtimeYieldCache[timeToWait];
        }
        else
        {
            yieldInstruction = new WaitForSecondsRealtime(timeToWait);
            realtimeYieldCache.Add(timeToWait, yieldInstruction);
        }
        return yieldInstruction;
    }

    IEnumerator WaitAndInvokeCoroutine(System.Action action, YieldInstruction delay)
    {
        yield return delay;
        action?.Invoke();
    }

    public Coroutine WaitAndInvoke<T>(System.Action<T> action, T obj, YieldInstruction delay) where T : Object
    {
        return StartCoroutine(WaitAndInvokeCoroutine(action, obj, delay));
    }

    IEnumerator WaitAndInvokeCoroutine<T>(System.Action<T> action, T obj, YieldInstruction delay) where T : Object
    {
        yield return delay;
        if (action != null)
        {
            action(obj);
        }
    }

    IEnumerator InvokeOnEndAnimation(Animator animator, string animation, System.Action action)
    {
        animator.Play(animation);
        while (!animator.GetCurrentAnimatorStateInfo(0).IsName(animation))
        {
            yield return null;
        }
        yield return GetYield(animator.GetCurrentAnimatorStateInfo(0).length);
        if (action != null)
        {
            action();
        }
    }

}
